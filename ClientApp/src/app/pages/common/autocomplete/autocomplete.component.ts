import {AfterViewInit, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {IPartialStockItem} from '../../../interfaces/common/i-partial-stock-item';
import {CommonDataService} from '../../../services/common-data.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements AfterViewInit, OnDestroy {

  @ViewChild('autocomplete', { static: false }) autocomplete;
  @Output() autocompleteRefEvent = new EventEmitter<any>();
  @Output() itemSKUEvent = new EventEmitter<IPartialStockItem>();

  unsubscribe$ = new Subject<void>();
  isLoading = false;
  keyword = "itemInfo";
  data: IPartialStockItem[] = [];

  constructor(private commonDataService: CommonDataService) { }

  ngAfterViewInit() {
    this.autocompleteRefEvent.emit(this.autocomplete);
  }

  onFocusedSearch(event) {
    this.isLoading = true;
    this.commonDataService.getMatchedStockItems()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IPartialStockItem[]) => {
        this.data = data;
        this.isLoading = false;
      });
  }

  onSelectItem(event: IPartialStockItem) {
    this.itemSKUEvent.emit(event);
    this.autocomplete.close();
  }

  onChangeSearch(event) {
    this.isLoading = true;
    this.commonDataService.getMatchedStockItems(event)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IPartialStockItem[]) => {
        this.data = data;
        this.isLoading = false;
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
