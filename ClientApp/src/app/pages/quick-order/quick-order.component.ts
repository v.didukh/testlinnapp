import {Component} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {IStockLocation} from '../../interfaces/common/i-stock-location';
import {CommonDataService} from '../../services/common-data.service';
import {DialogService} from '../../services/dialog.service';
import {Note} from '../../models/common/note';
import {map, skip, takeUntil} from 'rxjs/operators';
import {IPartialStockItem} from '../../interfaces/common/i-partial-stock-item';
import {ToastrService} from 'ngx-toastr';
import {OrderItemModel} from '../../models/common/order-item-model';
import {IOrderItemModel} from '../../interfaces/common/i-order-item-model';
import {TransmitterService} from '../../services/transmitter.service';
import {OrderService} from '../../services/order.service';
import {QuickOrder} from '../../models/quick-order';
import {Router} from '@angular/router';
import {QuickOrderServerResult} from '../../models/quick-order-server-result';
import {generateInvoiceByBase64} from '../../helpers/general-helpers';

@Component({
  selector: 'app-quick-order',
  templateUrl: './quick-order.component.html',
  styleUrls: ['./quick-order.component.css']
})
export class QuickOrderComponent {

  orderInCreateProcessing = false;

  unsubscribe$ = new Subject<void>();

  /* From autocomplete */
  stockItemId: string;
  autocomplete;

  /* Controls */
  customerNameControl = new FormControl('');
  companyControl = new FormControl('');
  postcodeControl = new FormControl('');
  addressControl = new FormControl('');
  locationControl = new FormControl('Default', [Validators.required]);
  statusControl = new FormControl('Unpaid', [Validators.required]);
  phoneControl = new FormControl('', [Validators.pattern("^[0-9]*$")]);
  emailControl = new FormControl('', [Validators.email]);

  statuses: Array<string> = [
    "Unpaid",
    "Paid",
    "Pending",
    "Resend"
  ];

  /* Slide toggles */
  isPrintInvoiceChecked = true;
  isRulesEngineChecked = true;
  isAutoProcessChecked = false;

  /* streams */
  stockLocations$: Observable<IStockLocation[]>;

  /* notes */
  notes: Note[] = [];

  /* service infos */
  //services: ServiceInfoModel[] = [];

  /* items */
  items: OrderItemModel[] = [];

  /* Grid api */
  gridApi;

  /* Location selected indicator (if true - select is disabled, otherwise is available) */
  locationSelectionAvailability = false;

  constructor(private commonDataService: CommonDataService,
              private dialogService: DialogService,
              private toastr: ToastrService,
              private transmitterService: TransmitterService,
              private orderService: OrderService,
              private router: Router) {
    this.stockLocations$ = this.commonDataService.getStockLocations();
  }

  openServiceModal(): void {
    this.dialogService.serviceModal()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((service: OrderItemModel) => {
        if(service !== undefined) {
          this.items.push(service);
          this.gridApi.api.setRowData(this.items);
        }
      });
  }

  openNotesModal(): void {
    this.dialogService.notesModal(this.notes)
      .pipe(
        takeUntil(this.unsubscribe$),
        map(response => response === undefined ? this.notes: response)
      )
      .subscribe((notes: Note[]) => {
          this.notes = notes;
      });
  }

  setGridApi(event) {
    this.gridApi = event;

    this.transmitterService.itemToRemove$
      .pipe(takeUntil(this.unsubscribe$), skip(1))
      .subscribe((item: OrderItemModel) => {
        let currentAvailable = item.available;
        const deletableItemIdx = this.items.indexOf(item);

        this.items = this.items.filter(x => x !== item);
        this.items.slice(deletableItemIdx).filter(x => x.sku === item.sku).forEach(x => {
          x.available = x.available + (currentAvailable - x.available);
          currentAvailable -= x.quantity;
        });

        if(!this.items.length) {
          this.locationSelectionAvailability = false;
        }
        this.gridApi.api.setRowData(this.items);
      });
  }

  setAutocompleteRef(event) {
    this.autocomplete = event;
  }

  getTotal(): number {
    return +this.items.reduce((acc, x) => acc += x.total, 0)
      .toFixed(2);
  }

  // Autocomplete event handler
  onSelectItem(event: IPartialStockItem) {
    this.stockItemId = event.stockItemId;

    this.commonDataService.getStockItemBySKU(this.stockItemId, this.locationControl.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((item: IOrderItemModel) => {
        if(item) {
          // Reversing array without mutating to get the last element with current sku to clone this element
          const similarItemByEnd = this.items.slice().reverse().find(x => x.sku === item.sku);
          if(similarItemByEnd) {
            const clone = new OrderItemModel(
              similarItemByEnd.stockItemId,
              similarItemByEnd.sku,
              similarItemByEnd.itemTitle,
              similarItemByEnd.unitCost,
              similarItemByEnd.taxRate,
              similarItemByEnd.available);

            clone.quantity = 1;
            clone.available = similarItemByEnd.available - similarItemByEnd.quantity;

            this.items.push(clone);
          }
          else {
            this.items.push(new OrderItemModel(item.stockItemId, item.sku, item.itemTitle, item.unitCost, item.taxRate, item.available));
          }
          // Block location selection list
          this.locationSelectionAvailability = true;
          this.gridApi.api.setRowData(this.items);

          this.autocomplete.clear();
          this.toastr.success("Item has been added successfully", null, { timeOut: 3000 });
        }
        else {
          this.toastr.error(`Item with ${this.stockItemId} SKU isn't found`, null, { timeOut: 3000 });
        }
      });
  }

  checkSubmitAvailable(): boolean {
    return (!this.customerNameControl.value || !this.companyControl.value) ||
      !this.addressControl.value || !this.postcodeControl.value || !this.items.length
      || this.emailControl.invalid || this.phoneControl.invalid || this.orderInCreateProcessing
      || this.items.some(x => x.available <= 0 && !x.isService);
  }

  onSubmitOrder(): void {
    const quickOrderInstance = new QuickOrder();
    quickOrderInstance.location = this.locationControl.value;
    quickOrderInstance.customerName = this.customerNameControl.value;
    quickOrderInstance.company = this.companyControl.value;
    quickOrderInstance.postcode = this.postcodeControl.value;
    quickOrderInstance.address = this.addressControl.value;
    quickOrderInstance.notes = this.notes;
    quickOrderInstance.items = this.items;
    quickOrderInstance.status = this.statusControl.value;
    quickOrderInstance.phone = +this.phoneControl.value;
    quickOrderInstance.email = this.emailControl.value;
    quickOrderInstance.printInvoice = this.isPrintInvoiceChecked;
    quickOrderInstance.runRulesEngine = this.isRulesEngineChecked;
    quickOrderInstance.autoProcessOrder = this.isAutoProcessChecked;

    this.orderInCreateProcessing = true;

    this.orderService.createQuickOrder(quickOrderInstance)
      .subscribe(
        (response: QuickOrderServerResult) => {

          this.toastr.success(`Order number: ${response.orderNumber}.
                                      Processed: ${response.isProcessed}.
                                      Ran Rules Engine: ${response.isRanRulesEngine}`,
            "Order has been created successfully");

          // INVOICE GENERATING
          if(response.invoiceInBase64) {
            generateInvoiceByBase64(response.invoiceInBase64);
          }

          this.clearFields();
          //this.router.navigate(['']);
          this.orderInCreateProcessing = false;
        },
        error => {
          this.toastr.error(error.message);
          this.orderInCreateProcessing = false;
        }
      );
  }

  clearFields(): void {
    this.customerNameControl.reset();
    this.companyControl.reset();
    this.postcodeControl.reset();
    this.addressControl.reset();
    this.phoneControl.reset();
    this.emailControl.reset();
    this.statusControl.setValue('Unpaid');
    this.locationControl.reset('Default');
    this.notes = [];
    this.items = [];
    this.isPrintInvoiceChecked = true;
    this.isRulesEngineChecked = true;
    this.isAutoProcessChecked = false;
    this.stockItemId = '';
    this.autocomplete.clear();
    this.locationSelectionAvailability = false;
    this.gridApi.api.setRowData([]);
  }
}
