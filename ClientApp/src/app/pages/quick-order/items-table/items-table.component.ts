import {Component, EventEmitter, Output} from '@angular/core';
import {ColDef} from 'ag-grid-community';
import {RemoveItemButtonComponent} from './remove-item-button/remove-item-button.component';
import {AvailableIndicatorComponent} from './available-indicator/available-indicator.component';

@Component({
  selector: 'app-items-table',
  templateUrl: './items-table.component.html'
})
export class ItemsTableComponent {

  @Output() gridApiEvent = new EventEmitter();

  columns: ColDef[] = [
    {
      cellRenderer: 'availableIndicator',
      colId: "availableIndicator",
      width: 80
    },
    { headerName: "SKU", field: "sku", sortable: true, resizable: true, filter: true },
    { headerName: "ItemTitle", field: "itemTitle", sortable: true, resizable: true, filter: true },
    { headerName: "Quantity", field: "quantity", sortable: true, resizable: true, editable: true, filter: true,
      valueParser: x => +x.newValue,
      onCellValueChanged: event => this.recountTotalAndRefreshCells(event)
    },
    { headerName: "Unit cost, £", field: "unitCost", sortable: true, resizable: true, filter: true,  valueParser: x => +x.newValue },
    { headerName: "Tax rate, %", field: "taxRate", sortable: true, resizable: true, filter: true,  valueParser: x => +x.newValue },
    { headerName: "Discount, %", field: "discount", sortable: true, resizable: true, editable: true, filter: true,
      valueParser: x => +x.newValue,
      onCellValueChanged: event => this.recountTotalAndRefreshCells(event)
    },
    { headerName: "Total, £", field: "total", sortable: true, resizable: true, filter: true,  valueParser: x => +x.newValue },
    {
      cellRenderer: 'removeItemButtonComponent',
      colId: "removeItem",
      width: 80
    }
  ];
  frameworkComponents = {
    'availableIndicator': AvailableIndicatorComponent,
    'removeItemButtonComponent': RemoveItemButtonComponent,
  };

  constructor() { }

  onGridReady(event) {
    this.gridApiEvent.emit(event);
  }

  recountTotalAndRefreshCells(event): void {
    event.data.countTotal();
    event.api.refreshCells();
  }

  onChangedRowsHandler(event) {
    // Auto-size all columns once the initial data is rendered.
    const colIds = event.columnApi
      .getAllDisplayedColumns()
      .map(col => col.getColId());
    event.columnApi.autoSizeColumns(colIds);

    // Expands columns to maximal table sizing
    event.api.sizeColumnsToFit();
  }
}
