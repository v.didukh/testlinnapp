import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';
import {TransmitterService} from '../../../../services/transmitter.service';

@Component({
  selector: 'app-remove-item-button',
  template: `
    <div style="text-align: center">
        <button mat-raised-button
                style="
                background-color: red;
                color: white; min-width: 20px;
                width: 20px; height: 25px;"
                (click)="onRemoveHandler($event)"
        >
        <i class="fas fa-trash-alt" style="padding-bottom: 12px; margin-left: -7px"></i>
      </button>
    </div>
  `,
})
export class RemoveItemButtonComponent implements ICellRendererAngularComp {

  private params: any;

  constructor(private transmitterService: TransmitterService) {
  }

  agInit(params: any): void {
    this.params = params;
  }

  onRemoveHandler(event) {
    this.transmitterService.setItemToRemove(this.params.data);
  }

  // Useless method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }
}
