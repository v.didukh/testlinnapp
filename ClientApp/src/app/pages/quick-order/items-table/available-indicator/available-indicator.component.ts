import {Component, EventEmitter, Output} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';
import {OrderItemModel} from '../../../../models/common/order-item-model';

@Component({
  selector: 'app-available-indicator',
  template: `
    <div *ngIf="!item.isService" style="text-align: center">
      <mat-checkbox
        color="primary"
        disabled
        [checked]="checked"
      >
      </mat-checkbox>
    </div>`
})
export class AvailableIndicatorComponent implements ICellRendererAngularComp {
  checked: boolean;
  item: OrderItemModel;

  agInit(params: ICellRendererParams): void {
    this.item = params.data;

    if(!this.item.isService) {
      if(!this.item.checkAvailable()) {
        this.checked = false;
      }
      else {
        this.checked = true;
      }
    }
  }

  // Useless interface method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }
}
