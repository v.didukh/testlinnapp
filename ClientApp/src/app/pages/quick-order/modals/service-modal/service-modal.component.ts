import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {OrderItemModel} from '../../../../models/common/order-item-model';

@Component({
  selector: "app-modal",
  templateUrl: "./service-modal.component.html",
  styleUrls: ["./service-modal.component.css"],
})
export class ServiceModalComponent {

  serviceFormGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    cost: new FormControl(0, [Validators.required, Validators.pattern("^[0-9]*$")]),
    tax: new FormControl(0, [Validators.required, Validators.pattern("^[0-9]*$")]),
    discount: new FormControl(0, [Validators.required, Validators.pattern("^[0-9]*$")])
  });

  constructor(private dialogRef: MatDialogRef<ServiceModalComponent>,
              private toastr: ToastrService) {

  }

  onSubmitForm() {
    if(this.serviceFormGroup.status === "VALID") {
      const value = this.serviceFormGroup.value;
      const serviceInstance = new OrderItemModel(null, null, value.name, +value.cost, +value.tax, null, true, +value.discount);
      this.dialogRef.close(serviceInstance);
      this.toastr.success("Service saved successfully",null, { timeOut: 3000 });
    }
    else {
      this.toastr.error("Please, fill required fields","Form cannot be saved", { timeOut: 3000 });
    }
  }

  closeModal(event) {
    event.preventDefault();
    this.dialogRef.close();
  }
}
