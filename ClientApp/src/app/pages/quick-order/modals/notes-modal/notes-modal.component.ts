import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material';
import {Note} from '../../../../models/common/note';
import {TransmitterService} from '../../../../services/transmitter.service';
import {Subject} from 'rxjs';
import {skip, takeUntil} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-notes-modal',
  templateUrl: './notes-modal.component.html',
  styleUrls: ['./notes-modal.component.css']
})
export class NotesModalComponent implements OnDestroy {

  unsubscribe$ = new Subject<void>();
  notes: Note[];

  gridApi;

  constructor(@Inject(MAT_DIALOG_DATA) private data: Note[],
              private dialogRef: MatDialogRef<NotesModalComponent>,
              private transmitterService: TransmitterService,
              private toastr: ToastrService) {
    // Clone an array and each of the objects, cause all of them represent the reference type
    this.notes = Object.assign([], this.data.map(x => Object.assign({}, x)));
  }

  addNote() {
    this.notes.push(new Note());
    this.gridApi.api.setRowData(this.notes);
  }

  setGridApi(event) {
    this.gridApi = event;

    // Table required subscribes, for data correctly working
    this.transmitterService.noteToRemove$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((note: Note) => {
        this.notes = this.notes.filter(x => x !== note);
        this.gridApi.api.setRowData(this.notes);
      });

    // Checkboxes updated subscribe
    this.transmitterService.triggerTable$
      .pipe(
        skip(1),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(_ => {
        this.gridApi.api.setRowData(this.notes);
      });
  }

  saveNotes() {
    if(this.notes.some(x => x.noteText === '')) {
      this.toastr.error("No one field cannot be empty", null, { timeOut: 3500 });
    }
    else {
      this.dialogRef.close(this.notes);
      this.toastr.success("Notes saved successfully");
    }
  }

  closeModal() {
    this.dialogRef.close(this.data);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
