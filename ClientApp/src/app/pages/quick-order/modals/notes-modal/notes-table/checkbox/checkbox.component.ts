import { Component } from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';
import {TransmitterService} from '../../../../../../services/transmitter.service';
import {Note} from '../../../../../../models/common/note';

@Component({
  selector: 'app-checkbox',
  template: `
    <div style="text-align: center">
      <mat-checkbox
        [name]="checkBoxName"
        color="primary"
        (change)="onChangeHandler($event)"
        [checked]="currentCheckboxValue"
      >
      </mat-checkbox>
    </div>`
})
export class CheckboxComponent implements ICellRendererAngularComp {

  private params: any;
  checkBoxName: string;
  currentNote: Note;
  currentCheckboxValue: boolean;

  constructor(private transmitterService: TransmitterService) { }

  agInit(params: ICellRendererParams): void {
    this.currentCheckboxValue = params.value;
    this.currentNote = params.data;
    this.checkBoxName = params.colDef.colId;
    this.params = params;
  }

  // Cause it is reference type, we can just change the value in an object
  onChangeHandler(event) {
    const checkBoxName = event.source.name;
    const value = event.checked;

    this.currentNote[checkBoxName] = value;
    this.transmitterService.triggerAgGridTable();
  }

  // Useless interface method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }
}
