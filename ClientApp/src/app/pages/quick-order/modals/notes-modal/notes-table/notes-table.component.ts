import {Component, EventEmitter, Output} from '@angular/core';
import {ColDef} from 'ag-grid-community';
import {RemoveButtonComponent } from './remove-button/remove-button.component';
import {CheckboxComponent} from './checkbox/checkbox.component';

@Component({
  selector: 'app-notes-table',
  templateUrl: './notes-table.component.html'
})
export class NotesTableComponent {

  @Output() gridApiEvent = new EventEmitter();

  columns: ColDef[] = [
    { field: "date", sortable: true, resizable: true },
    { field: "noteText", sortable: true, resizable: true, editable: true },
    { field: "internal", resizable: true, colId: "internal", cellRenderer: 'checkboxComponent' },
    { field: "processingNote", resizable: true, colId: "processingNote", cellRenderer: 'checkboxComponent' },
    {
      headerName: "",
      cellRenderer: 'removeButtonComponent',
      editable: false,
      colId: "remove",
      width: 80
    }
  ];
  frameworkComponents = {
    'removeButtonComponent': RemoveButtonComponent,
    'checkboxComponent': CheckboxComponent
  };

  onGridReady(event) {
    this.gridApiEvent.emit(event);
  }

  onChangedRowsHandler(event) {
    // Auto-size all columns once the initial data is rendered.
    const colIds = event.columnApi
      .getAllDisplayedColumns()
      .map(col => col.getColId());
    event.columnApi.autoSizeColumns(colIds);

    // Expands columns to maximal table sizing
    event.api.sizeColumnsToFit();
  }
}
