import {Component, EventEmitter, Output} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';
import {Note} from '../../../../../../models/common/note';
import {TransmitterService} from '../../../../../../services/transmitter.service';

@Component({
  selector: 'app-remove-button',
  template: `
    <div style="text-align: center">
        <button mat-raised-button
                style="
                background-color: red;
                color: white; min-width: 20px;
                width: 20px; height: 25px;"
                (click)="onRemoveHandler($event)"
        >
        <i class="fas fa-times" style="padding-bottom: 12px; margin-left: -5px"></i>
      </button>
    </div>
  `,
})
export class RemoveButtonComponent implements ICellRendererAngularComp {

  private params: any;

  constructor(private transmitterService: TransmitterService) {
  }

  agInit(params: any): void {
    this.params = params;
  }

  onRemoveHandler(event) {
    this.transmitterService.setNoteToRemove(this.params.data);
  }

  // Useless method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }
}
