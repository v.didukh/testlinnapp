import {Component, EventEmitter, Output} from '@angular/core';
import {ColDef} from 'ag-grid-community';
import {FulfilledIndicatorComponent} from './fulfilled-indicator/fulfilled-indicator.component';
import {AddressInfoComponent} from './address-info/address-info.component';
import {ItemsInfoComponent} from './items-info/items-info.component';
import {ActionOrderComponent} from './action-order/action-order.component';
import {FilteredOrderItem} from '../../../models/filtered-order';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-helpdesk-table',
  templateUrl: './helpdesk-table.component.html',
  styles: [
    ".ag-theme-balham::ng-deep .ag-cell-wrapper { display: grid; }"
  ]
})
export class HelpdeskTableComponent {

  @Output() gridApiEvent = new EventEmitter();

  columns: ColDef[] = [
    { headerName: "#", field: "orderNumber", sortable: true, resizable: true, filter: true, autoHeight: true,
      cellRenderer: params => {
        return `
        <div style="text-align: center; padding: 65px 0;">
            <p>${params.data.orderNumber}</p>
        </div>
      `;
      }},
    {
      headerName: 'Fulfilled',
      cellRenderer: 'fulfilledIndicator',
      colId: "fulfilledIndicator",
      autoHeight: true
    },
    { headerName: "Address", cellRenderer: 'addressInfo', autoHeight: true },
    { headerName: "Item", cellRenderer: 'itemsInfo', autoHeight: true, resizable: true },
    { headerName: "Received", field: "receivedDate", sortable: true, resizable: true, autoHeight: true, filter: true,
      cellRenderer: params => {
        return `
        <div style="text-align: center; padding: 20px 0;">
            <p>${formatDate(params.data.receivedDate, "dd MMMM", "en-US")}</p>
            <p>${formatDate(params.data.receivedDate, "yyyy", "en-US")}</p>
            <p>${formatDate(params.data.receivedDate, "hh:mm:ss", "en-US")}</p>
        </div>
      `;
      } },
    { headerName: "Line total", field: "lineTotal", sortable: true, resizable: true, autoHeight: true, filter: true,
      valueParser: x => +x.newValue,
      cellRenderer: params => {
        return `
          <div style="text-align: center; padding: 65px 0;">
              <p>${params.data.lineTotal} ${params.data.currency}</p>
          </div>
        `;
      }
    },
    { headerName: "Action", cellRenderer: 'actionOrder', autoHeight: true, flex: 1, resizable: true, cellStyle: params => {
        if(params.data.items.some((x: FilteredOrderItem) => x.available <= 0)) {
          return {
            'background-color': '#A54CFF'
          };
        }
        else {
          return {
            'background-color': '#6DD900'
          };
        }
      }
    },
  ];
  frameworkComponents = {
    'fulfilledIndicator': FulfilledIndicatorComponent,
    'addressInfo': AddressInfoComponent,
    'itemsInfo': ItemsInfoComponent,
    'actionOrder': ActionOrderComponent
  };

  onGridReady(event) {
    this.gridApiEvent.emit(event);
  }

  onChangedRowsHandler(event) {
    // Auto-size all columns once the initial data is rendered.
    const colIds = event.columnApi
      .getAllDisplayedColumns()
      .map(col => col.getColId());
    event.columnApi.autoSizeColumns(colIds);

    // Expands columns to maximal table sizing
    event.api.sizeColumnsToFit();
  }
}
