import { Component } from '@angular/core';
import {ICellRendererParams} from 'ag-grid-community';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {FilteredOrder, FilteredOrderItem} from '../../../../models/filtered-order';

@Component({
  selector: 'app-fulfilled-indicator',
  template: `
    <div style="text-align: center; padding: 65px 0;">
      <mat-checkbox
        color="primary"
        disabled
        [checked]="checked"
      >
      </mat-checkbox>
    </div>`
})
export class FulfilledIndicatorComponent implements ICellRendererAngularComp {
  checked: boolean;
  order: FilteredOrder;

  agInit(params: ICellRendererParams): void {
    this.order = params.data;

    if(this.order.items.some((x: FilteredOrderItem) => x.available <= 0)) {
      this.checked = false;
    }
    else {
      this.checked = true;
    }
  }

  // Useless interface method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }
}
