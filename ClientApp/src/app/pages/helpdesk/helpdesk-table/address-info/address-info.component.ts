import { Component } from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {FilteredOrder} from '../../../../models/filtered-order';
import {ICellRendererParams} from 'ag-grid-community';

@Component({
  selector: 'app-address-info',
  template: `
    <div style="padding: 10px 0;">
      <p *ngIf="order.customer">{{order.customer}}</p>
      <p *ngIf="order.email">{{order.email}}</p>
      <p *ngIf="order.phone">{{order.phone}}</p>
      <p *ngIf="order.address">{{order.address}}</p>
      <p *ngIf="order.city">{{order.city}}</p>
      <p *ngIf="order.country">{{order.country}}</p>
    </div>
  `
})
export class AddressInfoComponent implements ICellRendererAngularComp {
  order: FilteredOrder;

  agInit(params: ICellRendererParams): void {
    this.order = params.data;
  }

  // Useless interface method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }
}
