import { Component, OnInit } from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';
import {FilteredOrder} from '../../../../models/filtered-order';

@Component({
  selector: 'app-items-info',
  template: `
    <div>
      <p *ngFor="let item of order.items" [ngStyle]="{ 'color': item.available <= 0 ? 'red': 'black '}">
        {{item.sku}} - {{item.qty}}
      </p>
    </div>
  `
})
export class ItemsInfoComponent implements ICellRendererAngularComp {
  order: FilteredOrder;

  agInit(params: ICellRendererParams): void {
    this.order = params.data;
  }

  // Useless interface method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }
}
