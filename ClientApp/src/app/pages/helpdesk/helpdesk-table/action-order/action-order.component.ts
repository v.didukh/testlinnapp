import {Component, OnDestroy} from '@angular/core';
import {FilteredOrder, FilteredOrderItem} from '../../../../models/filtered-order';
import {ICellRendererParams} from 'ag-grid-community';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {map, takeUntil} from 'rxjs/operators';
import {Note} from '../../../../models/common/note';
import {DialogService} from '../../../../services/dialog.service';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {OrderService} from '../../../../services/order.service';
import {ICreatePurchaseResult} from '../../../../interfaces/i-create-purchase-result';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-action-order',
  template: `
    <div (click)="onActionHandler($event)" style="text-align: center; padding: 65px 0; color: white">
        <p>{{actionName}}</p>
    </div>
  `,
  styleUrls: ['./action-order.component.css']
})
export class ActionOrderComponent implements ICellRendererAngularComp, OnDestroy {
  order: FilteredOrder;
  actionName: string;
  unsubscribe$ = new Subject<void>();

  constructor(private dialogService: DialogService,
              private orderService: OrderService,
              private snackBar: MatSnackBar) {
  }

  agInit(params: ICellRendererParams): void {
    this.order = params.data;

    if(this.order.items.some((x: FilteredOrderItem) => x.available <=0)) {
      this.actionName = "Create PO";
    }
    else {
      this.actionName = "Process";
    }
  }

  onActionHandler(event) {
    switch (this.actionName) {
      case "Create PO": {
        this.createPurchaseOrder()
          .subscribe((purchaseResult: ICreatePurchaseResult) => {
          if(purchaseResult.alreadyPurchased) {
            this.snackBar.open("Purchase Order has been already created.", null, { duration: 3000 });
          }
          else {
            this.snackBar.open(`PO\`s have been created: ${(purchaseResult.purchaseNumbers.join(", "))}.`, null, { duration: 3000 });
          }
        });
        break;
      }
      case "Process": {
        this.openNotesModal(this.order);
        break;
      }
      default: {
        break;
      }
    }
  }

  private createPurchaseOrder(): Observable<ICreatePurchaseResult> {
      const itemsToPurchase = this.order.items.filter(x => x.available <= 0);
      return this.orderService.createPurchaseOrder(this.order.orderId, itemsToPurchase);
  }

  private openNotesModal(order: FilteredOrder): void {
    this.dialogService.orderProcessModal(order)
      .pipe(
        takeUntil(this.unsubscribe$),
      )
      .subscribe();
  }

  // Useless interface method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
