import {Component, Inject, OnDestroy} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import {OrderService} from '../../../services/order.service';
import {IOrderAdditionalInfo} from '../../../interfaces/i-order-additional-info';
import {ToastrService} from 'ngx-toastr';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {generateInvoiceByBase64, generateShippingLabelByBase64} from '../../../helpers/general-helpers';
import {IInvoiceResult} from '../../../interfaces/common/i-invoice-result';
import {IProcessOrderResult} from '../../../interfaces/i-process-order-result';
import {IShippingLabelResult} from '../../../interfaces/i-shipping-label-result';

@Component({
  selector: 'app-process-modal',
  templateUrl: './process-modal.component.html',
  styleUrls: ['./process-modal.component.css']
})
export class ProcessModalComponent implements OnDestroy {

  unsubscribe$ = new Subject<void>();
  order: IOrderAdditionalInfo;
  invoice: IInvoiceResult;
  shippingLabel: IShippingLabelResult;

  constructor(private dialogRef: MatDialogRef<ProcessModalComponent>,
              @Inject(MAT_DIALOG_DATA) private orderId: string,
              private orderService: OrderService,
              private toastr: ToastrService,
              private snackBar: MatSnackBar) {
    this.orderService.getOrderAdditionalInfo(orderId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((order: IOrderAdditionalInfo) => {
      this.order = order;
    }, error => this.toastr.error(error));
  }

  printInvoice() {
    this.orderService.printInvoice(this.orderId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((invoice: IInvoiceResult) => {
        this.invoice = invoice;
        generateInvoiceByBase64(this.invoice.invoiceInBase64);
      }, error => this.toastr.error("Printing invoice process threw an error."));
  }

  processOrder() {
    this.orderService.processOrder(this.orderId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((processedResult: IProcessOrderResult) => {
        if(processedResult.processed && !processedResult.error) {
          this.toastr.success("An order has been processed successfully.", null, { timeOut: 3000 });
          this.dialogRef.close();
        }
        if(processedResult.error) {
          this.snackBar.open(processedResult.error, null, { duration: 3000 });
        }
      }, error => this.toastr.error(`Something went wrong while order was processing. Message -> ${error}`));
  }

  printShippingLabel() {
    this.orderService.printShippingLabel(this.orderId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((shippingLabelResult: IShippingLabelResult) => {
        this.shippingLabel = shippingLabelResult;
        generateShippingLabelByBase64(this.shippingLabel.shippingLabelInBase64);
      }, error => this.toastr.error(`Printing shipping label process threw an error. Message -> ${error}`));
  }

  closeModal() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
