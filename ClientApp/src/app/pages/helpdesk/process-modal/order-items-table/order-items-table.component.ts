import {Component, Input} from '@angular/core';
import {ColDef} from 'ag-grid-community';
import {OrderItemAdditionalInfo} from '../../../../interfaces/i-order-additional-info';
import {MainImageComponent} from './main-image/main-image.component';

@Component({
  selector: 'app-order-items-table',
  templateUrl: './order-items-table.component.html'
})
export class OrderItemsTableComponent {

  @Input() items: OrderItemAdditionalInfo[];
  domLayout = "autoHeight";

  columns: ColDef[] = [
    { headerName: "", field: "mainImageInBase64", resizable: true, autoHeight: true, cellRenderer: 'mainImage' },
    { headerName: "SKU", field: "sku", sortable: true, resizable: true, filter: true },
    { headerName: "Barcode", field: "barcodeNumber", sortable: true, resizable: true, filter: true },
    { headerName: "Title", field: "title", sortable: true, resizable: true, filter: true },
    { headerName: "Quantity", field: "quantity", sortable: true, resizable: true, filter: true },
    { headerName: "Per unit", field: "pricePerUnit", sortable: true, resizable: true, filter: true },
    { headerName: "Total cost", field: "total", sortable: true, resizable: true, filter: true }
  ];
  frameworkComponents = {
    'mainImage': MainImageComponent,
  };

  onChangedRowsHandler(event) {
    // Auto-size all columns once the initial data is rendered.
    const colIds = event.columnApi
      .getAllDisplayedColumns()
      .map(col => col.getColId());
    event.columnApi.autoSizeColumns(colIds);

    // Expands columns to maximal table sizing
    event.api.sizeColumnsToFit();
  }
}
