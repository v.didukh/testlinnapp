import { Component } from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';
import {OrderItemAdditionalInfo} from '../../../../../interfaces/i-order-additional-info';

@Component({
  selector: 'app-main-image',
  template: `
    <div *ngIf="item.mainImageInBase64">
      <img [src]="src" alt="Item image" />
    </div>
  `,
})
export class MainImageComponent implements ICellRendererAngularComp {

  item: OrderItemAdditionalInfo;
  src: string;

  constructor() { }

  agInit(params: ICellRendererParams): void {
    this.item = params.data;
    this.src = `data:image/png;base64,${this.item.mainImageInBase64}`;
  }

  // Useless interface method
  refresh(params: ICellRendererParams): boolean {
    return false;
  }

}
