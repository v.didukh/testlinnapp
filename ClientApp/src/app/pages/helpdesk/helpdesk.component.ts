import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {IStockLocation} from '../../interfaces/common/i-stock-location';
import {CommonDataService} from '../../services/common-data.service';
import {ISource} from '../../interfaces/common/i-source';
import {IPartialStockItem} from '../../interfaces/common/i-partial-stock-item';
import {OrderService} from '../../services/order.service';
import {formatDate} from '@angular/common';
import {FilteredOrder, FilteredOrderItem} from '../../models/filtered-order';
import {ToastrService} from 'ngx-toastr';
import {ICsvResult} from '../../interfaces/common/i-csv-result';
import {takeUntil} from 'rxjs/operators';

import { saveAs } from 'file-saver';
import str2ab from 'string-to-arraybuffer';

@Component({
  selector: 'app-helpdesk',
  templateUrl: './helpdesk.component.html',
  styleUrls: ['./helpdesk.component.css']
})
export class HelpdeskComponent implements AfterViewInit, OnDestroy {

  unsubscribe$ = new Subject<void>();
  stockLocations$: Observable<IStockLocation[]>;
  sources$: Observable<ISource[]>;

  isProcessed = false;

  locationControl = new FormControl('Default', [Validators.required]);
  statusControl = new FormControl('Paid', [Validators.required]);
  dateFromControl = new FormControl('', [Validators.required]);
  dateToControl = new FormControl('', [Validators.required]);
  sourceControl = new FormControl('DIRECT', [Validators.required]);
  subSourceControl = new FormControl('');
  countryControl = new FormControl('');
  dateTypeControl = new FormControl('CreatedDate', [Validators.required]);
  ordersAmountControl = new FormControl(1, [Validators.required, Validators.pattern("^[0-9]*$")]);

  statuses: Array<string> = [
    "Unpaid",
    "Paid",
    "Pending",
    "Resend"
  ];

  dateTypes: Array<string> = [
    "dReceievedDate",
    "dProcessedOn",
    "CreatedDate",
    "dDispatchBy"
  ];

  stockItemId = "";
  autocomplete;

  orders: FilteredOrder[] = [];

  gridApi;

  constructor(private commonDataService: CommonDataService,
              private orderService: OrderService,
              private toastr: ToastrService) {
    this.stockLocations$ = this.commonDataService.getStockLocations();
    this.sources$ = this.commonDataService.getSources();
  }

  ngAfterViewInit(): void {
    this.autocomplete.focus();
  }

  setGridApi(event) {
    this.gridApi = event;
  }

  onSelectItem(event: IPartialStockItem) {
    this.stockItemId = event.stockItemId;
  }

  setAutocompleteRef(event) {
    this.autocomplete = event;
  }

  getCsvFile() {
    const dateFrom = this.dateToControl.value.toISOString();
    const dateTo = this.dateFromControl.value.toISOString();

    this.orderService.generateCsvForFilteredOrders(dateFrom, dateTo, this.orders)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((csvResult: ICsvResult) => {
        const bufferArray = str2ab(csvResult.bufferArray);
        saveAs(
          new Blob([bufferArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }),
          csvResult.fileName);

      }, error => this.toastr.error(error));
  }

  getOrders() {
    this.orderService.getFilteredOrders(this.locationControl.value,
        this.sourceControl.value,
        this.subSourceControl.value,
        this.countryControl.value,
        formatDate(this.dateFromControl.value, "dd/MM/yyyy", "en-US"),
        formatDate(this.dateToControl.value, "dd/MM/yyyy", "en-US"),
        this.statusControl.value,
        this.dateTypeControl.value,
        this.stockItemId,
        this.isProcessed,
        this.ordersAmountControl.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: FilteredOrder[]) => {
        this.orders = data;

        if(!this.orders.length) {
          this.gridApi.api.setRowData([]);
          this.toastr.warning("There is no data.", null, { timeOut: 3000 });
        }
        else {
          this.gridApi.api.setRowData(this.orders);
          this.toastr.success("Data has received successfully.", null, { timeOut: 3000 });
        }
      }, error =>
        this.toastr.error(`Something went wrong while receiving data. Message: ${error}`, null, { timeOut: 3000 }));
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
