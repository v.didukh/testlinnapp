import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html'
})
export class DataTableComponent implements OnInit {

  @Input() columns;
  @Input() data;
  @Input() rowClassRules;

  constructor() { }

  ngOnInit() {
  }

  autoSizeColumns(event) {
    // Auto-size all columns once the initial data is rendered.
    const colIds = event.columnApi
      .getAllDisplayedColumns()
      .map(col => col.getColId());
    event.columnApi.autoSizeColumns(colIds);

    // Expands columns to maximal table sizing
    event.api.sizeColumnsToFit();
  }
}
