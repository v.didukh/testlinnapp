import {Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewChild} from '@angular/core';
import {formatDate} from '@angular/common';
import {Reports} from '../../../common/reports.enum';
import {ReportService} from '../../../services/report.service';
import {ColDef} from 'ag-grid-community';
import {IItalyPaidOrderReport} from '../../../interfaces/reports/i-italy-paid-order-report';
import {ToastrService} from 'ngx-toastr';
import {IPurchaseOrderWhichArrivedTodayReport} from '../../../interfaces/reports/i-purchase-order';
import {IStartWarsItemReport} from '../../../interfaces/reports/i-start-wars-item-report';
import {IReturnsRefundsReport} from '../../../interfaces/reports/i-returns-refunds-report';
import {IStockReport} from '../../../interfaces/reports/i-stock-report';
import {IItemsByDefaultSuppReport} from '../../../interfaces/reports/i-items-by-default-supp-report';
import {IPurchaseAsDeliveredSoonReport} from '../../../interfaces/reports/i-purchase-as-delivered-soon-report';
import * as XLSX from 'xlsx';
import {CommonDataService} from '../../../services/common-data.service';
import {IFolder} from '../../../interfaces/common/IFolder';
import {Observable, Subject} from 'rxjs';
import {IStockLocation} from '../../../interfaces/common/i-stock-location';
import {ISupplier} from '../../../interfaces/common/ISupplier';
import {FormControl, Validators} from '@angular/forms';
import {ICategory} from '../../../interfaces/common/ICategory';
import {takeUntil} from 'rxjs/operators';
import {IPartialStockItem} from '../../../interfaces/common/i-partial-stock-item';

@Component({
  selector: 'app-specific-report',
  templateUrl: './specific-report.component.html',
  styleUrls: ['./specific-report.component.css']
})
export class SpecificReportComponent implements OnChanges, OnDestroy {

  private unsubscribe$: Subject<void> = new Subject<void>();

  @Input() report: string;
  @Input() reportTitle: string;
  reports = Reports;

  /* Ag-Grid requirements */
  rowClassRules = {};
  agColumns: ColDef[] = [];
  rowData = [];

  /* Common required data */
  folders$: Observable<IFolder[]>;
  stockLocations$: Observable<IStockLocation[]>;
  suppliers$: Observable<ISupplier[]>;
  categories$: Observable<ICategory[]>;

  /* Italy report params */
  folderControl = new FormControl('', Validators.required);

  /* Start wars items report params */
  startDate_1_6: Date = null;

  /* Start wars items report params */
  dateFrom_1_8: Date = null;
  dateTo_1_8: Date = null;

  /* Stock report params */
  locationControl = new FormControl('', Validators.required);
  categoryControl = new FormControl('', Validators.required);

  /* Get items by supplier report params */
  supplierControl = new FormControl('', Validators.required);

  /* Purchase items by itemSKU, which will be delivered soon params */
  itemSKU_1_11 = "";

  constructor(private reportService: ReportService, private commonDataService: CommonDataService, private toastr: ToastrService)
  {
    this.folders$ = this.commonDataService.getFolders();
    this.stockLocations$ = this.commonDataService.getStockLocations();
    this.suppliers$ = this.commonDataService.getSuppliers();
    this.categories$ = this.commonDataService.getCategories();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.rowClassRules = {};
    this.categoryControl.setValue('');
    this.locationControl.setValue('');
    this.supplierControl.setValue('');
    this.folderControl.setValue('');

    this.dateFrom_1_8 = null;
    this.dateTo_1_8 = null;
    this.startDate_1_6 = null;

    this.clearColumns();
    this.clearRowsData();
  }

  showReport() {
    this.clearColumns();
    switch (this.report) {
      case Reports.ItalyPaidOrders_1_1:
        this.getItalyPaidOrders();
        break;
      case Reports.PurchaseOrdersWhichArrivedToday_1_3:
        this.getPurchaseOrdersWhichWillBeArrivedToday();
        break;
      case Reports.StartWarsItemsReport_1_6:
        this.getStartWarsItem();
        break;
      case Reports.ReturnsAndRefundsReport_1_8:
        this.getReturnsAndRefunds();
        break;
      case Reports.StockReport_1_9:
        this.getStock();
        break;
      case Reports.ItemsByDefaultSupplierReport_1_10:
        this.getItemsByDefaultSupplier();
        break;
      case Reports.PurchaseOrdersByItemAsDeliveredSoon_1_11:
        this.getPurchaseOrdersByItemAsDeliveredSoon();
        break;
      default:
        console.log("No one isn't correct.");
    }
  }

  exportCsv() {
    if(this.rowData.length > 0) {
      try {
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        const ws: XLSX.WorkSheet =  XLSX.utils.json_to_sheet(this.rowData);
        XLSX.utils.book_append_sheet(wb, ws, 'Report');

        // paid functionality
        for (let i = 1; i <= this.rowData.length; i++) {
          ws["A"+ i].s = {
            fill: {
              fgColor: {
                rgb: "FFFFCC00"
              }
            }
          };
        }

        const currentDate = formatDate(Date.now(), "dd/MM/yyyy hh:MM:ss", "en-US");
        XLSX.writeFile(wb, `Report_${this.reportTitle}_${currentDate}.xls`);
        this.toastr.success("File has been downloaded");
      }
      catch(error) {
        console.log(error);
        this.toastr.error("Something went wrong while downloading");
      }
    }
    else {
      this.toastr.warning("There aren't any data in a table");
    }
  }

  fillColumns(columns : string[]): ColDef[] {
    const columnsOfInterfaceProps: ColDef[] = [];
    for(const column of columns) {
      columnsOfInterfaceProps.push({ field: column, filter: 'agSetColumnFilter', sortable: true, resizable: true });
    }
    return columnsOfInterfaceProps;
  }

  fillRows<T>(items: T[]): any[] {
    const rowData = [];
    for(const item of items) {
      const row = {};
      for (const column of this.agColumns) {
        if(column.field.includes("Date")) {
          row[column.field] = formatDate(item[column.field], "dd/MM/yyyy hh:MM:ss", "en-US");
        }
        else {
          row[column.field] = item[column.field];
        }
      }
      rowData.push(row);
    }
    return rowData;
  }

  clearColumns(): void {
    this.agColumns = [];
  }

  clearRowsData(): void {
    this.rowData = [];
  }

  getItalyPaidOrders(): void {
    this.reportService.getItalyPaidOrdersReport(this.folderControl.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IItalyPaidOrderReport[]) => {
        if(data.length > 0) {
          this.agColumns = this.fillColumns(Object.keys(data[0]));
          this.rowData = this.fillRows<IItalyPaidOrderReport>(data);
        }
        this.toastr.success("Data received successfully", "Success", { timeOut: 3000 });
      }, error => {
        console.error(error);
        this.toastr.error("Check the required parameters.", "Error", { timeOut: 3500 });
      });
  }

  getPurchaseOrdersWhichWillBeArrivedToday(): void {
    this.reportService.getPurchaseOrdersWhichWillBeArrivedTodayReport()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IPurchaseOrderWhichArrivedTodayReport[]) => {
        if(data.length > 0) {
          this.agColumns = this.fillColumns(Object.keys(data[0]));
          this.rowData = this.fillRows<IPurchaseOrderWhichArrivedTodayReport>(data);
        }
        this.toastr.success("Data received successfully", "Success", { timeOut: 3000 });
      }, error => {
        console.error(error);
        this.toastr.error("Something went wrong. Try again later.", "Error", { timeOut: 3500 });
      });
  }

  getStartWarsItem(): void {
    this.reportService.getStartWarsItemsReport(this.startDate_1_6)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IStartWarsItemReport[]) => {
        if(data.length > 0) {
          this.agColumns = this.fillColumns(Object.keys(data[0]));
          this.rowData = this.fillRows<IStartWarsItemReport>(data);
        }
        this.toastr.success("Data received successfully", "Success", { timeOut: 3000 });
      }, error => {
        console.error(error);
        this.toastr.error("Check the required parameters.", "Error", { timeOut: 3500 });
      });
  }

  getReturnsAndRefunds(): void {
    console.log(this.dateFrom_1_8, this.dateTo_1_8);
    this.reportService.getReturnsAndRefundsReport(this.dateFrom_1_8, this.dateTo_1_8)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IReturnsRefundsReport[]) => {
        if(data.length > 0) {
          this.agColumns = this.fillColumns(Object.keys(data[0]));
          this.rowData = this.fillRows<IReturnsRefundsReport>(data);
        }
        this.toastr.success("Data received successfully", "Success", { timeOut: 3000 });
      }, error => {
        console.error(error);
        this.toastr.error("Check the required parameters.", "Error", { timeOut: 3500 });
      });
  }

  getStock() {
    this.reportService.getStockReport(this.locationControl.value, this.categoryControl.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IStockReport[]) => {
        if(data.length > 0) {
          this.agColumns = this.fillColumns(Object.keys(data[0]));
          this.rowData = this.fillRows<IStockReport>(data);
        }
        this.toastr.success("Data received successfully", "Success", { timeOut: 3000 });
      }, error => {
        console.error(error);
        this.toastr.error("Check the required parameters.", "Error", { timeOut: 3500 });
      });
  }

  getItemsByDefaultSupplier(): void {
    this.reportService.getItemsByDefaultSupplierReport(this.supplierControl.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IItemsByDefaultSuppReport[]) => {
        if(data.length > 0) {
          this.agColumns = this.fillColumns(Object.keys(data[0]));
          this.rowData = this.fillRows<IItemsByDefaultSuppReport>(data);
        }
        this.toastr.success("Data received successfully", "Success", { timeOut: 3000 });
      }, error => {
        console.error(error);
        this.toastr.error("Check the required parameters.", "Error", { timeOut: 3500 });
      });
  }

  getPurchaseOrdersByItemAsDeliveredSoon(): void {
    this.reportService.getPurchaseOrdersByItemAsDeliveredSoonReport(this.itemSKU_1_11)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: IPurchaseAsDeliveredSoonReport[]) => {
        if(data.length > 0) {
          this.rowClassRules = {
              'green-row': function(params) {
                const expectedDate = params.data.expectedDate.substr(0, 10);
                const today = formatDate(Date.now(), "dd/MM/yyyy", "en-US");
                return expectedDate === today;
              },
              'yellow-row': function(params) {
                const expectedDate = params.data.expectedDate.substr(0, 10);
                const today = formatDate(Date.now(), "dd/MM/yyyy", "en-US");
                return expectedDate !== today;
              }
          };
          this.agColumns = this.fillColumns(Object.keys(data[0]));
          this.rowData = this.fillRows<IPurchaseAsDeliveredSoonReport>(data);
        }
        this.toastr.success("Data received successfully", "Success", { timeOut: 3000 });
      }, error => {
        console.error(error);
        this.toastr.error("Check the required parameters.", "Error", { timeOut: 3500 });
      });
  }

  // Autocomplete event handler
  onSelectItem(event: IPartialStockItem) {
    this.itemSKU_1_11 = event.itemInfo;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
