import {Component, OnDestroy} from '@angular/core';
import {Reports} from '../../common/reports.enum';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnDestroy {

  currentReport = null;
  navigationSubscription: Subscription;
  reportTitle = "";

  constructor(private route: ActivatedRoute, private router: Router) {
    // Event, which triggering when reports url /:id is changing
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.currentReport = this.route.snapshot.params['id'];
        // Previous version of title (just a key of enum)
        //this.reportTitle = Object.keys(Reports)[Object.values(Reports).indexOf(this.currentReport)];
        // Changing the report title to more prettier version
        switch (this.currentReport) {
          case Reports.ItalyPaidOrders_1_1:
            this.reportTitle = "Italy paid orders";
            break;
          case Reports.PurchaseOrdersWhichArrivedToday_1_3:
            this.reportTitle = "Purchase orders which will be arrived today";
            break;
          case Reports.StartWarsItemsReport_1_6:
            this.reportTitle = "Star Wars items report";
            break;
          case Reports.ReturnsAndRefundsReport_1_8:
            this.reportTitle = "Returns and refunds report";
            break;
          case Reports.StockReport_1_9:
            this.reportTitle = "Stock report";
            break;
          case Reports.ItemsByDefaultSupplierReport_1_10:
            this.reportTitle = "Items by specific supplier report";
            break;
          case Reports.PurchaseOrdersByItemAsDeliveredSoon_1_11:
            this.reportTitle = "Purchase orders by SKU, which will be delivered soon";
            break;
          default:
            console.log("No one chosen report isn't correct.");
        }
      }
    });
  }

  ngOnDestroy() {
    this.navigationSubscription.unsubscribe();
  }
}
