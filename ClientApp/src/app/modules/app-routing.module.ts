import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from '../pages/main/main.component';
import {ReportsComponent} from '../pages/reports/reports.component';
import {QuickOrderComponent} from '../pages/quick-order/quick-order.component';
import {HelpdeskComponent} from '../pages/helpdesk/helpdesk.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'quickOrder', component: QuickOrderComponent },
  { path: 'reports/:id', component: ReportsComponent },
  { path: 'helpdesk', component: HelpdeskComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const RoutingComponents = [MainComponent, ReportsComponent, QuickOrderComponent, HelpdeskComponent];
