import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AuthenticationInterceptor} from './interceptors/authentication.interceptor';
import { ToastrModule } from 'ngx-toastr';
import {AppRoutingModule, RoutingComponents} from './modules/app-routing.module';
import {MaterialModule} from './modules/material.module';
import {MatNativeDateModule} from '@angular/material/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SpecificReportComponent } from './pages/reports/specific-report/specific-report.component';
import { DataTableComponent } from './pages/reports/data-table/data-table.component';
import {AgGridModule} from 'ag-grid-angular';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { QuickOrderComponent } from './pages/quick-order/quick-order.component';
import { ItemsTableComponent } from './pages/quick-order/items-table/items-table.component';
import { ServiceModalComponent } from './pages/quick-order/modals/service-modal/service-modal.component';
import { NotesModalComponent } from './pages/quick-order/modals/notes-modal/notes-modal.component';
import { NotesTableComponent } from './pages/quick-order/modals/notes-modal/notes-table/notes-table.component';
import { AutocompleteComponent } from './pages/common/autocomplete/autocomplete.component';
import { RemoveButtonComponent } from './pages/quick-order/modals/notes-modal/notes-table/remove-button/remove-button.component';
import { CheckboxComponent } from './pages/quick-order/modals/notes-modal/notes-table/checkbox/checkbox.component';
import { RemoveItemButtonComponent } from './pages/quick-order/items-table/remove-item-button/remove-item-button.component';
import { AvailableIndicatorComponent } from './pages/quick-order/items-table/available-indicator/available-indicator.component';
import { HelpdeskComponent } from './pages/helpdesk/helpdesk.component';
import { HelpdeskTableComponent } from './pages/helpdesk/helpdesk-table/helpdesk-table.component';
import { FulfilledIndicatorComponent } from './pages/helpdesk/helpdesk-table/fulfilled-indicator/fulfilled-indicator.component';
import { AddressInfoComponent } from './pages/helpdesk/helpdesk-table/address-info/address-info.component';
import { ItemsInfoComponent } from './pages/helpdesk/helpdesk-table/items-info/items-info.component';
import { ActionOrderComponent } from './pages/helpdesk/helpdesk-table/action-order/action-order.component';
import {LoaderInterceptor} from './interceptors/loader.interceptor';
import {ProcessModalComponent} from './pages/helpdesk/process-modal/process-modal.component';
import {OrderItemsTableComponent} from './pages/helpdesk/process-modal/order-items-table/order-items-table.component';
import { MainImageComponent } from './pages/helpdesk/process-modal/order-items-table/main-image/main-image.component';

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,
    SpecificReportComponent,
    DataTableComponent,
    ItemsTableComponent,
    ServiceModalComponent,
    NotesModalComponent,
    NotesTableComponent,
    AutocompleteComponent,
    RemoveButtonComponent,
    CheckboxComponent,
    RemoveItemButtonComponent,
    AvailableIndicatorComponent,
    HelpdeskTableComponent,
    FulfilledIndicatorComponent,
    ProcessModalComponent,
    AddressInfoComponent,
    ItemsInfoComponent,
    ActionOrderComponent,
    OrderItemsTableComponent,
    MainImageComponent
  ],
    imports: [
        CommonModule,
        BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
        HttpClientModule,
        FormsModule,
        AppRoutingModule,
        MaterialModule,
        MatNativeDateModule,
        FontAwesomeModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        AgGridModule.withComponents([
          RemoveButtonComponent,
          CheckboxComponent,
          RemoveItemButtonComponent,
          AvailableIndicatorComponent,
          FulfilledIndicatorComponent,
          AddressInfoComponent,
          ItemsInfoComponent,
          ActionOrderComponent,
          MainImageComponent
        ]),
        ReactiveFormsModule,
        AutocompleteLibModule
    ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    }
  ],
  entryComponents: [
    ServiceModalComponent,
    NotesModalComponent,
    ProcessModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
