import * as printJS from 'print-js';

export function generateInvoiceByBase64(base64String: string): void {
  printJS({ printable: base64String, type: 'pdf', base64: true });
}

export function generateShippingLabelByBase64(base64String: string): void {
  printJS({ printable: base64String, type: 'pdf', base64: true });
}
