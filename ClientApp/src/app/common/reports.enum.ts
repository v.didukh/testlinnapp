export enum Reports {
  ItalyPaidOrders_1_1 = "7d880e3a-4fe5-4b33-8112-e1005a57b6f5",
  PurchaseOrdersWhichArrivedToday_1_3 = "38e5500c-007b-484b-8c5e-e363b7bba969",
  StartWarsItemsReport_1_6 = "86f8b423-3a02-4c51-96fc-4be1b7f4f37a",
  ReturnsAndRefundsReport_1_8 = "25af4936-149f-4514-bb01-17cd6a190ca9",
  StockReport_1_9 = "7df1b7dd-102a-4d40-a90c-6246e2e553ad",
  ItemsByDefaultSupplierReport_1_10 = "2a6eaa48-f0c3-48ef-b8e1-b10368d1312d",
  PurchaseOrdersByItemAsDeliveredSoon_1_11 = "75dc3b50-cbca-450e-8ee8-be5d9d25beed"
}
