import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {QuickOrder} from '../models/quick-order';
import {Observable} from 'rxjs';
import {QuickOrderServerResult} from '../models/quick-order-server-result';
import {FilteredOrder, FilteredOrderItem} from '../models/filtered-order';
import {IOrderAdditionalInfo} from '../interfaces/i-order-additional-info';
import {IInvoiceResult} from '../interfaces/common/i-invoice-result';
import {IProcessOrderResult} from '../interfaces/i-process-order-result';
import {IShippingLabelResult} from '../interfaces/i-shipping-label-result';
import {ICreatePurchaseResult} from '../interfaces/i-create-purchase-result';
import {ICsvResult} from '../interfaces/common/i-csv-result';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  createQuickOrder(orderRequest: QuickOrder): Observable<QuickOrderServerResult> {
    return this.http.post<QuickOrderServerResult>("/api/orders/createQuickOrder", orderRequest);
  }

  getFilteredOrders(location: string,
                    source: string,
                    subSource: string,
                    country: string,
                    dateFrom: string,
                    dateTo: string,
                    status: string,
                    filterBy: string,
                    itemId: string,
                    processed: boolean,
                    ordersAmount: number): Observable<FilteredOrder[]> {
    return this.http.get<FilteredOrder[]>(`/api/orders/getFilteredOrders?filterBy=${filterBy}&status=${status}&location=${location}&source=${source}&subSource=${subSource}&dateFrom=${dateFrom}&dateTo=${dateTo}&itemId=${itemId}&country=${country}&processed=${processed}&ordersAmount=${ordersAmount}`);
  }

  getOrderAdditionalInfo(orderId: string): Observable<IOrderAdditionalInfo> {
    return this.http.get<IOrderAdditionalInfo>(`/api/orders/${orderId}`);
  }

  printInvoice(orderId: string): Observable<IInvoiceResult> {
    return this.http.get<IInvoiceResult>(`/api/orders/printInvoice/${orderId}`);
  }

  processOrder(orderId: string): Observable<IProcessOrderResult> {
    return this.http.post<IProcessOrderResult>(`/api/orders/processOrder/${orderId}`, null);
  }

  printShippingLabel(orderId: string): Observable<IShippingLabelResult> {
    return this.http.get<IShippingLabelResult>(`/api/orders/printShippingLabel/${orderId}`);
  }

  createPurchaseOrder(orderId: string, itemsToPurchase: FilteredOrderItem[]): Observable<ICreatePurchaseResult> {
    return this.http.post<ICreatePurchaseResult>(`/api/orders/createPurchaseOrder?orderId=${orderId}`,
      { itemsIds: itemsToPurchase.map(x => x.itemId) });
  }

  generateCsvForFilteredOrders(dateFrom: string, dateTo: string, orders: FilteredOrder[]): Observable<ICsvResult> {
    return this.http.post<ICsvResult>('/api/orders/generateCsv', { orders, dateFrom, dateTo });
  }
}
