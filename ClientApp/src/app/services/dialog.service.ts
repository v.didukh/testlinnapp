import { Injectable } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { Observable } from "rxjs";
import {ServiceModalComponent} from '../pages/quick-order/modals/service-modal/service-modal.component';
import {NotesModalComponent} from '../pages/quick-order/modals/notes-modal/notes-modal.component';
import {Note} from '../models/common/note';
import {OrderItemModel} from '../models/common/order-item-model';
import {ProcessModalComponent} from '../pages/helpdesk/process-modal/process-modal.component';
import {FilteredOrder} from '../models/filtered-order';

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  constructor(protected dialog: MatDialog) {}

  public serviceModal(): Observable<OrderItemModel> {
    let dialogRef: MatDialogRef<ServiceModalComponent>;
    dialogRef = this.dialog.open(ServiceModalComponent, {
      width: "300px",
      height: "460px"
    });
    return dialogRef.afterClosed();
  }

  public notesModal(notes: Note[]): Observable<Note[]> {
    let dialogRef: MatDialogRef<NotesModalComponent>;
    dialogRef = this.dialog.open(NotesModalComponent,{
      width: "850px",
      height: "500px",
      data: notes
    });

    return dialogRef.afterClosed();
  }

  public orderProcessModal(order: FilteredOrder): Observable<void> {
    let dialogRef: MatDialogRef<ProcessModalComponent>;
    dialogRef = this.dialog.open(ProcessModalComponent,{
      width: "1450px",
      height: "650px",
      disableClose: true,
      data: order.orderId
    });

    return dialogRef.afterClosed();
  }
}
