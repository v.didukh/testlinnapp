import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IItalyPaidOrderReport} from '../interfaces/reports/i-italy-paid-order-report';
import {Observable} from 'rxjs';
import {IPurchaseOrderWhichArrivedTodayReport} from '../interfaces/reports/i-purchase-order';
import {IStartWarsItemReport} from '../interfaces/reports/i-start-wars-item-report';
import {IReturnsRefundsReport} from '../interfaces/reports/i-returns-refunds-report';
import {IStockReport} from '../interfaces/reports/i-stock-report';
import {IItemsByDefaultSuppReport} from '../interfaces/reports/i-items-by-default-supp-report';
import {IPurchaseAsDeliveredSoonReport} from '../interfaces/reports/i-purchase-as-delivered-soon-report';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) { }

  getItalyPaidOrdersReport(folder: string): Observable<IItalyPaidOrderReport[]> {
    return this.http.get<IItalyPaidOrderReport[]>(`/api/reports/italyPaidOrders?folder=${folder}`);
  }

  getPurchaseOrdersWhichWillBeArrivedTodayReport(): Observable<IPurchaseOrderWhichArrivedTodayReport[]> {
    return this.http.get<IPurchaseOrderWhichArrivedTodayReport[]>('/api/reports/purchaseOrdersArrivedToday');
  }

  getStartWarsItemsReport(startDate: Date): Observable<IStartWarsItemReport[]> {
    return this.http.post<IStartWarsItemReport[]>('/api/reports/startWarsItemsReport', { startDate });
  }

  getReturnsAndRefundsReport(dateFrom: Date, dateTo: Date): Observable<IReturnsRefundsReport[]> {
    return this.http.post<IReturnsRefundsReport[]>('/api/reports/returnsAndRefundsReport', { dateFrom, dateTo });
  }

  getStockReport(location: string, category: string): Observable<IStockReport[]> {
    return this.http.post<IStockReport[]>('/api/reports/stockReport', { location, category });
  }

  getItemsByDefaultSupplierReport(supplier: string): Observable<IItemsByDefaultSuppReport[]> {
    return this.http.get<IItemsByDefaultSuppReport[]>(`/api/reports/itemsByDefaultSupplier?supplier=${supplier}`);
  }

  getPurchaseOrdersByItemAsDeliveredSoonReport(itemSku: string): Observable<IPurchaseAsDeliveredSoonReport[]> {
    return this.http.post<IPurchaseAsDeliveredSoonReport[]>('/api/reports/purchaseOrdersByItem', { itemSku });
  }
}
