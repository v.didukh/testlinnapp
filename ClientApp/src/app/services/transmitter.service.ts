import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Note} from '../models/common/note';
import {OrderItemModel} from '../models/common/order-item-model';

@Injectable({
  providedIn: 'root'
})
export class TransmitterService {
  public noteToRemove$: Observable<Note>;
  private noteToRemove: BehaviorSubject<Note>;

  // public checkboxTransmittedValue$: Observable<Note>;
  // private checkboxTransmittedValue: BehaviorSubject<Note>;

  public triggerTable$: Observable<void>;
  private triggerTable: BehaviorSubject<void>;

  public itemToRemove$: Observable<OrderItemModel>;
  private itemToRemove: BehaviorSubject<OrderItemModel>;

  // public itemWrapper$: Observable<StockItemWrapper>;
  // private itemWrapper: BehaviorSubject<StockItemWrapper>;

  constructor() {
    this.noteToRemove = new BehaviorSubject<Note>(null);
    this.noteToRemove$ = this.noteToRemove.asObservable();

    // this.checkboxTransmittedValue = new BehaviorSubject<Note>(null);
    // this.checkboxTransmittedValue$ = this.checkboxTransmittedValue.asObservable();

    this.triggerTable = new BehaviorSubject<void>(null);
    this.triggerTable$ = this.triggerTable.asObservable();

    this.itemToRemove = new BehaviorSubject<OrderItemModel>(null);
    this.itemToRemove$ = this.itemToRemove.asObservable();

    // this.itemWrapper = new BehaviorSubject<StockItemWrapper>(null);
    // this.itemWrapper$ = this.itemWrapper.asObservable();
  }

  setNoteToRemove(note: Note): void {
    this.noteToRemove.next(note);
  }

  // setNoteCheckBoxValue(updatedNote: Note) {
  //   this.checkboxTransmittedValue.next(updatedNote);
  // }

  // setItemWrapper(ItemWrapper: StockItemWrapper) {
  //   this.itemWrapper.next(ItemWrapper);
  // }

  setItemToRemove(item: OrderItemModel) {
    this.itemToRemove.next(item);
  }

  triggerAgGridTable() {
    this.triggerTable.next();
  }
}
