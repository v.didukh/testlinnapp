import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ISession} from '../interfaces/common/ISession';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly baseUrl: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  authorize(): Observable<ISession> {
    return this.http.post<ISession>('/api/auth', null);
  }
}
