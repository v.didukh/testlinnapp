import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IFolder} from '../interfaces/common/IFolder';
import {IStockLocation} from '../interfaces/common/i-stock-location';
import {ISupplier} from '../interfaces/common/ISupplier';
import {ICategory} from '../interfaces/common/ICategory';
import {IPartialStockItem} from '../interfaces/common/i-partial-stock-item';
import {IOrderItemModel} from '../interfaces/common/i-order-item-model';
import {ISource} from '../interfaces/common/i-source';

@Injectable({
  providedIn: 'root'
})
export class CommonDataService {

  constructor(private http: HttpClient) { }

  getFolders(): Observable<IFolder[]> {
    return this.http.get<IFolder[]>('/api/common/folders');
  }

  getStockLocations(): Observable<IStockLocation[]> {
    return this.http.get<IStockLocation[]>('/api/common/stockLocations');
  }

  getSuppliers(): Observable<ISupplier[]> {
    return this.http.get<ISupplier[]>('/api/common/suppliers');
  }

  getCategories(): Observable<ICategory[]> {
    return this.http.get<ICategory[]>('/api/common/categories');
  }

  getMatchedStockItems(match: string = ""): Observable<IPartialStockItem[]> {
    return this.http.get<IPartialStockItem[]>(`/api/common/matchedStockItems?match=${match}`);
  }

  getStockItemBySKU(stockItemId: string, location: string): Observable<IOrderItemModel> {
    return this.http.get<IOrderItemModel>(`/api/common/stockItem?stockItemId=${stockItemId}&location=${location}`);
  }

  getSources(): Observable<ISource[]> {
    return this.http.get<ISource[]>("/api/common/getSources");
  }
}
