import {Component} from '@angular/core';
import {ISession} from './interfaces/common/ISession';
import {AuthService} from './services/auth.service';
import {Reports as ReportEnum} from './common/reports.enum';
import {LoaderService} from './services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['/app.component.css']
})
export class AppComponent {

  private session: ISession;
  showFiller = false;
  enumReportsKeys = [];
  reports = ReportEnum;

  constructor(private authService: AuthService,
              public loaderService: LoaderService) {
    this.enumReportsKeys=Object.keys(ReportEnum);
    this.authorizationByApp();
  }

  authorizationByApp(): void {
    if (sessionStorage.getItem("userId") === null ||
      sessionStorage.getItem("accessToken") === null ||
      sessionStorage.getItem("email") === null)
    {
      setTimeout(() => {
        this.authService.authorize()
          .subscribe(
            (credentials: ISession) => {
              this.session = credentials;
              sessionStorage.setItem("userId", this.session.userId);
              sessionStorage.setItem("accessToken", this.session.token);
              sessionStorage.setItem("email", this.session.email);
            }, error => console.error(error));
      }, 500);
    }
  }
}
