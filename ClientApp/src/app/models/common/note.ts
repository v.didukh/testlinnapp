import {formatDate} from '@angular/common';

export class Note {
  readonly date: string;
  noteText: string;
  internal: boolean;
  processingNote: boolean; // opens modal window before order will have been processed

  constructor() {
    this.date = formatDate(Date.now(), "dd/MM/yyyy hh:MM:ss", "en-US");
    this.noteText = "";
    this.internal = true;
    this.processingNote = false;
  }
}
