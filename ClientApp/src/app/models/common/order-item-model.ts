import {IOrderItemModel} from '../../interfaces/common/i-order-item-model';

export class OrderItemModel implements IOrderItemModel {
  stockItemId: string;
  sku: string;
  itemTitle: string;
  taxRate: number;
  unitCost: number;
  quantity: number;
  discount: number;
  total: number;
  available: number;
  isService: boolean;

  constructor(_stockItemId: string = null,
              _sku: string,
              _itemTitle: string,
              _unitCost: number,
              _taxRate: number,
              _available: number = null,
              _isService = false,
              _discount: number = 0) {
    this.stockItemId = _stockItemId;
    this.sku = _sku;
    this.itemTitle = _itemTitle;
    this.unitCost = _unitCost;
    this.taxRate = _taxRate;
    this.quantity = 1;
    this.discount = _discount;
    this.countTotal();
    this.available = _available; // cause by default item is added with 1 started quantity
    this.isService = _isService;
  }

  countTotal(): void {
    const taxPercentage = this.taxRate > 0 ? this.taxRate/100: 1;
    const discountPercentage = this.discount > 0 ? this.discount/100: 1;
    const count = (this.unitCost + (this.unitCost * taxPercentage * discountPercentage)) * this.quantity;
    this.total = +count.toFixed(2);
  }

  checkAvailable(): boolean {
    return (this.available - +this.quantity) >= 0;
  }
}
