import {Guid} from 'guid-typescript';

export class FilteredOrderItem {
  itemId: Guid;
  sku: string;
  qty: number;
  available: number;

  constructor(itemId: Guid,
              sku: string,
              qty: number,
              available: number) {
    this.itemId = itemId;
    this.sku = sku;
    this.qty = qty;
    this.available = available;
  }
}

export class FilteredOrder {
  orderId: string;
  orderNumber: string;
  receivedDate: string;
  lineTotal: number;
  customer: string;
  currency: string;
  email: string;
  phone: string;
  address: string;
  isProcessed: boolean;
  city: string;
  country: string;
  items: FilteredOrderItem[];

  constructor(orderId: string,
  orderNumber: string,
  receivedDate: string,
  lineTotal: number,
  customer: string,
  email: string,
  phone: string,
  address: string,
  city: string,
  country: string,
  currency: string,
   isProcessed: boolean,
  items: FilteredOrderItem[]) {
    this.orderId = orderId;
    this.orderNumber = orderNumber;
    this.country = country;
    this.items = items;
    this.city = city;
    this.email = email;
    this.receivedDate = receivedDate;
    this.customer = customer;
    this.lineTotal = lineTotal;
    this.currency = currency;
    this.address = address;
    this.isProcessed = isProcessed;
  }
}
