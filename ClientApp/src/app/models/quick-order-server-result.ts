export class QuickOrderServerResult {
  orderNumber: number;
  invoiceInBase64: string;
  isProcessed: boolean;
  isRanRulesEngine: boolean;
}
