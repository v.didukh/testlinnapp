import {Note} from './common/note';
import {OrderItemModel} from './common/order-item-model';

export class QuickOrder {
  location: string;
  customerName: string;
  company: string;
  postcode: string;
  address: string;
  notes: Note[];
  items: OrderItemModel[];
  status: string;
  phone: number;
  email: string;
  printInvoice: boolean;
  runRulesEngine: boolean;
  autoProcessOrder: boolean;
}
