import { Guid } from "guid-typescript";

export interface OrderItemAdditionalInfo {
    itemId: number;
    sku: number;
    barcodeNumber: number;
    title: number;
    quantity: number;
    pricePerUnit: number;
    total: number;
    mainImageInBase64: string;
}

export interface IOrderAdditionalInfo {
  orderId: Guid;
  address: string;
  fullName: string;
  phoneNumber: string;
  emailAddress: string;
  town: string;
  postCode: string;
  country: string;
  shippingMethod: string;
  packaging: string;
  packageType: string;
  trackingNumber: string;
  currency: string;
  itemWeight: number;
  totalWeight: number;
  postageCostExTax: number;
  tax: number;
  total: number;
  items: OrderItemAdditionalInfo[];
}
