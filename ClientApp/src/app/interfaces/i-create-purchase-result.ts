export interface ICreatePurchaseResult {
  alreadyPurchased: boolean;
  purchaseNumbers: string[];
}
