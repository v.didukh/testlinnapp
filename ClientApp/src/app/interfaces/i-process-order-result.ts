import {Guid} from 'guid-typescript';

export interface IProcessOrderResult {
  orderId: Guid;
  processed: boolean;
  error: string;
}
