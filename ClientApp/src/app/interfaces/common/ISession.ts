import {Guid} from 'guid-typescript';

export interface ISession {
  userId: string;
  token: string;
  email: string;
}
