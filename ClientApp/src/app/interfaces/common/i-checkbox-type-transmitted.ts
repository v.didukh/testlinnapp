import {Note} from '../../models/common/note';

// Obsolete
export interface ICheckboxTypeTransmitted {
  checkBoxName: string;
  value: boolean;
  currentNote: Note;
}
