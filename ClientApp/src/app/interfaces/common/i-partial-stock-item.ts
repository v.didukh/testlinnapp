export interface IPartialStockItem {
  stockItemId: string;
  itemInfo: string; // {SKU}-{BARCODE}
}
