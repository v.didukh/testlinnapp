export interface ICsvResult {
  bufferArray: string;
  fileName: string;
}
