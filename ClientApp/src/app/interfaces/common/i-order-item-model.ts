export interface IOrderItemModel {
  stockItemId: string;
  sku: string;
  itemTitle: string;
  unitCost: number;
  taxRate: number;
  available: number;
  isService: boolean;
}
