export interface IItemsByDefaultSuppReport {
  sku: string;
  description: string;
  category: string;
  knownPurchasePrice: number;
  barcode: string;
  weight: number;
  height: number;
  depth: number;

  CTN_QTY: number;
  CBM_PER_QTY: number;
  commodityCode: number;
}
