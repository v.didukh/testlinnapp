export interface IItalyPaidOrderReport {
  orderNumber: string;
  createdDate: Date;
  currency: string;
}
