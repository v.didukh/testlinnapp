export interface IPurchaseAsDeliveredSoonReport {
  poNumber: string;
  expectedDate: Date;
  quantity: number;
  cost: number;

  CTN_QTY: number;
  CBM_PER_QTY: number;
  TotalCBM: number;
}
