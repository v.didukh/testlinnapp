export interface IStartWarsItemReport {
  sku: string;
  salesUA: number;
  soldUA: number;
  salesIT: number;
  soldIT: number;
  salesUK: number;
  soldUK: number;
  salesPaypal: number;
  salesCash: number;
  totalSales: number;
  totalSold: number;

  CTN_QTY: number;
  CBM_PER_QTY: number;
  TotalCBM: number;
}
