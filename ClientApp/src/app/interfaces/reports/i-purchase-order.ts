export interface IPurchaseOrderWhichArrivedTodayReport {
  sku: string;
  reference: string;
  location: string;
  address1: string;
  town: string;
  country: string;
  supplierName: string;
  supplierRef: string;
  currency: string;
  title: string;
  supplierCode: string;
  supplierBarcode: string;
  quantity: number;
  unitCost: number;
  tax: number;
  cost: number;
}
