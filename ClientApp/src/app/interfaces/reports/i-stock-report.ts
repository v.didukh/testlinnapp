export interface IStockReport {
  sku: string;
  description: string;
  category: string;
  barcode: string;
  retailPrice: number;
  purchasePrice: number;
  minLevel: number;
  stockLevel: number;
  available: number;
  stockValue: number;
  weight: number;
  height: number;
  depth: number;

  CTN_QTY: number;
  CBM_PER_QTY: number;
  TotalCBM: number;
}
