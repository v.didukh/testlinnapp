export interface IReturnsRefundsReport {
  sku: string;
  barcode: string;
  description: string;
  source: string;
  subSource: string;
  fullName: string;
  email: string;
  address: string;
  city: string;
  postalCode: string;
  reference: string;
  externalReference: string;

  orderDate: Date;
  processDate: Date;
  returnDate: Date;
  refundGiven: number;
  currency: string;
  reason: string;
}
