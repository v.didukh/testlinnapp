import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(!req.url.includes("auth")) {
      req = req.clone({
        setHeaders: {
          Authorization: sessionStorage.getItem("accessToken")
        }
      });
    }
    return next.handle(req);
  }
}
