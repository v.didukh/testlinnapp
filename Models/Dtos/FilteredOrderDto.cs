﻿using System;
using System.Collections.Generic;

namespace TestApp.Models.Dtos
{
    public class FilteredOrderItemDto
    {
        public string ItemId { get; set; }
        public string SKU { get; set; }
        public int Qty { get; set; }
        public int Available { get; set; }
    }

    public class FilteredOrderDto
    {
        public string OrderId { get; set; }
        public string OrderNumber { get; set; }
        public DateTime ReceivedDate { get; set; }
        public double LineTotal { get; set; }
        public string Customer { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Currency { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public bool IsProcessed { get; set; }
        public List<FilteredOrderItemDto> Items { get; set; }
    }
}
