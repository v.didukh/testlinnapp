﻿namespace TestApp.Models.Common
{
    public class StockItemPartialDto
    {
        public string StockItemId { get; set; }
        public string ItemInfo { get; set; } // {SKU}-{BARCODE}
    }
}
