﻿namespace TestApp.Models.Common
{
    public class CategoryDto
    {
        public string CategoryName { get; set; }
    }
}
