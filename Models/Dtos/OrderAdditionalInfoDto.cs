﻿using System;
using System.Collections.Generic;

namespace TestApp.Models.Dtos
{
    public class OrderItemAdditionalInfoDto
    {
        public Guid ItemId { get; set; }
        public string SKU { get; set; } 
        public string BarcodeNumber { get; set; }
        public string Title { get; set; }
        public int Quantity { get; set; }
        public double PricePerUnit { get; set; }
        public double Total { get; set; } 
        public string MainImageInBase64 { get; set; }
    }

    public class OrderAdditionalInfoDto
    {
        public Guid OrderId { get; set; }
        public string Address { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string ShippingMethod { get; set; }
        public string Packaging { get; set; }
        public string PackageType { get; set; }
        public string TrackingNumber { get; set; }
        public string Currency { get; set; }
        public double ItemWeight { get; set; }
        public double TotalWeight { get; set; }
        public double PostageCostExTax { get; set; }
        public double Tax { get; set; }
        public double Total { get; set; }
        public List<OrderItemAdditionalInfoDto> Items { get; set; }
    }
}
