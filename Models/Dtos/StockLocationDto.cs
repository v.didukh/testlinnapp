﻿namespace TestApp.Models.Common
{
    public class StockLocationDto
    {
        public string LocationName { get; set; }
    }
}
