﻿using System;

namespace TestApp.Models.Common
{
    public class StockItemDto
    {
        public string StockItemId { get; set; }
        public string SKU { get; set; }
        public string ItemTitle { get; set; }
        public double UnitCost { get; set; }
        public double TaxRate { get; set; }
        public int Available { get; set; }
    }
}
