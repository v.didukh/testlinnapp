﻿using System;

namespace TestApp.Models.Common
{
    public class CountryModel
    {
        public Guid CountryID { get; set; }

        public string Continent { get; set; }
    }

}
