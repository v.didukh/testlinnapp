﻿using System;

namespace TestApp.Models.Common
{
    public class Note
    {
        public string Date { get; set; }
        public string NoteText { get; set; }
        public bool Internal { get; set; }
        public bool ProcessingNote { get; set; }
    }
}
