﻿using System;

namespace TestApp.Models.Common
{
    public class Supplier
    {
        public string SupplierId { get; set; }
        public string SupplierCode { get; set; }
        public int PackSize { get; set; }
    }
}
