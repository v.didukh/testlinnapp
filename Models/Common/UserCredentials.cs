﻿using System;
using System.Text.Json;

namespace TestApp.Dto
{
    public class UserCredentials
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }

        public UserCredentials(string userId, string token, string email)
        {
            UserId = userId;
            Token = token;
            Email = email;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
