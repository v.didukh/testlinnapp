﻿namespace TestApp.Models.Common
{
    public class ShippingLabelResult
    {
        public string ShippingLabelInBase64 { get; set; }
    }
}
