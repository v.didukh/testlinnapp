﻿using LinnworksAPI;
using System.Collections.Generic;

namespace TestApp.Models.Common
{
    public class InvoiceResult
    {
        public string InvoiceInBase64 { get; set; }
    }
}
