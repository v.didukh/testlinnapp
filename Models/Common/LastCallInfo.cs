﻿using System;

namespace TestApp.Models.Common
{
    public class LastCallInfo
    {
        public DateTime FirstRequestTime { get; set; }
        public int SentRequestsCount { get; set; }

    }
}
