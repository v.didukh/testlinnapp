﻿namespace TestApp.Models.Common
{
    public class CsvFileResult
    {
        public byte[] BufferArray { get; set; } 
        public string FileName { get; set; }
    }
}
