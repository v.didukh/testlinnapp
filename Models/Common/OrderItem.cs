﻿using System;

namespace TestApp.Models.Common
{
    public class OrderItem
    {
        public string StockItemId { get; set; }
        public string SKU { get; set; }
        public string ItemTitle { get; set; }
        public double TaxRate { get; set; }
        public double UnitCost { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }
        public double Total { get; set; }
        public int? Available { get; set; }
        public bool IsService { get; set; }
    }
}
