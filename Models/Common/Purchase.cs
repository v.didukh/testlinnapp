﻿using System;

namespace TestApp.Models.Common
{
    public class Purchase
    {
        public string StockItemId { get; set; }
        public string OrderNumber { get; set; }
        public string ItemNumber { get; set; }
        public string LocationId { get; set; }
        public string Currency { get; set; }
        public int Quantity { get; set; }
        public Models.Common.Supplier Supplier { get; set; }
        public decimal Cost { get; set; }
        public decimal TaxRate { get; set; }
    }
}
