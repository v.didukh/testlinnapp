﻿using System.ComponentModel.DataAnnotations;

namespace TestApp.Dto
{
    public class PurchaseOrdersByItemReportRequest
    {
        [Required]
        public string itemSKU { get; set; }
    }
}
