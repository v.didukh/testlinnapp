﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestApp.Dto
{
    public class ReturnsAndRefundsReportRequest
    {
        [Required]
        public DateTime DateFrom { get; set; }
        [Required]
        public DateTime DateTo { get; set; }
    }
}
