﻿using System;
using System.Collections.Generic;

namespace TestApp.Models.Requests
{
    public class OrderItemRequest
    {
        public string SKU { get; set; }
        public int Qty { get; set; }
        public int Available { get; set; }
    }

    public class OrderRequest
    {
        public string OrderNumber { get; set; }
        public DateTime ReceivedDate { get; set; }
        public double LineTotal { get; set; }
        public string Customer { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Currency { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public bool IsProcessed { get; set; }
        public List<OrderItemRequest> Items { get; set; }
    }

    public class OrdersRequest
    {
        public List<OrderRequest> Orders { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
