﻿using System.ComponentModel.DataAnnotations;

namespace TestApp.Dto
{
    public class StockReportRequest
    {
        [Required]
        public string Location { get; set; }
        [Required]
        public string Category { get; set; }
    }
}
