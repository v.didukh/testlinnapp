﻿using System.Collections.Generic;
using TestApp.Models.Common;

namespace TestApp.Models.Requests
{
    public sealed class QuickOrderRequest
    {
        public string Location { get; set; }
        public string CustomerName { get; set; }
        public string Company { get; set; }
        public string PostCode { get; set; }
        public string Address { get; set; }
        public List<Note> Notes { get; set; }
        public List<OrderItem> Items { get; set; }
        public string Status { get; set; }
        public int PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool PrintInvoice { get; set; }
        public bool RunRulesEngine { get; set; }
        public bool AutoProcessOrder { get; set; }
    }
}
