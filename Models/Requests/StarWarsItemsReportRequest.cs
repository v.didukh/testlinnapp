﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestApp.Dto
{
    public class StarWarsItemsReportRequest
    {
        [Required]
        public DateTime StartDate { get; set; } 
    }
}
