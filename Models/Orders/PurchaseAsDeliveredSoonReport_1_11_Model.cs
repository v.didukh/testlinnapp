﻿using System;

namespace TestApp.Models
{
    public class PurchaseAsDeliveredSoonReport_1_11_Model
    {
        public string PONumber { get; set; }
        public DateTime ExpectedDate { get; set; }
        public int Quantity { get; set; }
        public double Cost { get; set; }

        public int CTN_QTY { get; set; }
        public double CBM_PER_QTY { get; set; }
        public double TotalCBM { get; set; }
    }
}
