﻿using System;

namespace TestApp.Models
{
    public class ReturnsRefunds_1_8_Model
    {
        public string SKU { get; set; }
        public string Barcode { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string SubSource { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Reference { get; set; }
        public string ExternalReference { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime ProcessDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public double RefundGiven { get; set; }
        public string Currency { get; set; }
        public string Reason { get; set; }
    }
}
