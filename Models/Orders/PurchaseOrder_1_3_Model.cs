﻿namespace TestApp.Models
{
    public class PurchaseOrder_1_3_Model
    {
        public string SKU { get; set; }
        public string Reference  { get; set; }
        public string Location { get; set; }
        public string Address1 { get; set; }
        public string Town { get; set; }
        public string Country { get; set; }
        public string SupplierName { get; set; }
        public string SupplierRef { get; set; }
        public string Currency { get; set; }
        public string Title { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierBarcode { get; set; }
        public int Quantity { get; set; }
        public double UnitCost { get; set; }
        public double Tax { get; set; }
        public double Cost { get; set; }
    }
}
