﻿namespace TestApp.Models
{
    public class StockReport_1_9_Model
    {
        public string SKU { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Barcode { get; set; }
        public double RetailPrice { get; set; }
        public double PurchasePrice { get; set; }
        public int MinLevel { get; set; }
        public int StockLevel { get; set; }
        public int Available { get; set; }
        public double StockValue { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }
        public double Depth { get; set; }

        public int CTN_QTY { get; set; }
        public double CBM_PER_QTY { get; set; }
        public double TotalCBM { get; set; }
    }
}
