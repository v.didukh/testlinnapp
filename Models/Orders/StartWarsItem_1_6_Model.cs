﻿using System;

namespace TestApp.Models
{
    public class StartWarsItem_1_6_Model
    {
        public string SKU { get; set; }
        public double SalesUA { get; set; }
        public int SoldUA { get; set; }
        public double SalesIT { get; set; }
        public int SoldIT {  get; set; }
        public double SalesUK { get; set; }
        public int SoldUK { get; set; }
        public double SalesPaypal { get; set; }
        public double SalesCash { get; set; }
        public double TotalSales { get; set; }
        public int TotalSold {  get; set; }

        public int CTN_QTY { get; set; }
        public double CBM_PER_QTY { get; set; }
        public double TotalCBM { get; set; }
    }
}
