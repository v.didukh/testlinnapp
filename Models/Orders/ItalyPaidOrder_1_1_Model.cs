﻿using System;

namespace TestApp.Models
{
    public class ItalyPaidOrder_1_1_Model
    {
        public string OrderNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Currency { get; set; }
    }
}
