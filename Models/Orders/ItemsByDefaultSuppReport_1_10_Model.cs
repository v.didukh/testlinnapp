﻿namespace TestApp.Models
{
    public class ItemsByDefaultSuppReport_1_10_Model
    {
        public string SKU { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public double KnownPurchasePrice { get; set; }
        public string Barcode { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }
        public double Depth { get; set; }

        public int CTN_QTY { get; set; }
        public double CBM_PER_QTY { get; set; }
        public string CommodityCode { get; set; }
    }
}
