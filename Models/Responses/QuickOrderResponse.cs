﻿namespace TestApp.Models.Responses
{
    public class QuickOrderResponse
    {
        public int OrderNumber { get; set; }
        public string InvoiceInBase64 { get; set; }
        public bool IsProcessed { get; set; }
        public bool IsRanRulesEngine { get; set; }
    }
}
