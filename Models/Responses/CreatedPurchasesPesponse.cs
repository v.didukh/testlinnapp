﻿using System.Collections.Generic;

namespace TestApp.Models.Responses
{
    public class CreatedPurchasesPesponse
    {
        public bool AlreadyPurchased { get; set; }
        public List<string> PurchaseNumbers { get; set; }
    }
}
