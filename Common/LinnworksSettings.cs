﻿using System;

namespace TestApp.Common
{
    public sealed class LinnworksSettings
    {
        public string AuthApiUrl { get; set; }    
        public string DataApiUrl { get; set; }
        public Guid ApplicationId { get; set; }
        public Guid ApplicationSecret {  get; set; }
        public Guid ApplicationToken {  get; set; }
    }
}
