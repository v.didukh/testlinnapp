﻿namespace TestApp.Common.Enums
{
    public enum FilterTypes
    {
        dReceievedDate,
        dProcessedOn,
        CreatedDate,
        dDispatchBy
    }

    public enum StatusTypes
    {
        Unpaid = 0,
        Paid = 1,
        Pending = 2,
        Resend = 3
    }
}
