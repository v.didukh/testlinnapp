﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace TestApp.ModelBinders
{
    public class ArrayModelBinder : IModelBinder
    {
        private class Response
        {
            public Guid[] ItemsIds { get; set; }
        }
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (!bindingContext.ModelMetadata.IsEnumerableType)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.CompletedTask;
            }

            bindingContext.HttpContext.Request.EnableBuffering();
            var body = bindingContext.HttpContext.Request.Body;
            body.Position = 0;

            using StreamReader reader = new(body);
            Task<string> data = reader.ReadToEndAsync();

            var deserializedData = JsonSerializer.Deserialize<Response>(data.Result, new JsonSerializerOptions
            {
                 PropertyNameCaseInsensitive = true
            });
             
            bindingContext.Model = deserializedData.ItemsIds;
            bindingContext.Result = ModelBindingResult.Success(bindingContext.Model);

            return Task.CompletedTask;
        }
    }
}
