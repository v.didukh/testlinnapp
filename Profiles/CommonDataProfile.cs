﻿using AutoMapper;
using LinnworksAPI;
using System;
using TestApp.Models.Common;
using TestApp.Models.Dtos;

namespace TestApp.Profiles
{
    public class CommonDataProfile: Profile
    {
        public CommonDataProfile()
        {
            CreateMap<InventoryStockLocation, StockLocationDto>();
            CreateMap<OrderFolder, OrderFolderDto>();
            CreateMap<LinnworksAPI.Supplier, SupplierDto>();
            CreateMap<Category, CategoryDto>();
        }
    }
}
