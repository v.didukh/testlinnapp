﻿using AutoMapper;
using LinnworksAPI;
using System;
using TestApp.Models.Dtos;

namespace TestApp.Profiles
{
    public class OrdersProfile: Profile
    {
        public OrdersProfile()
        {
            CreateMap<OrderItem, OrderItemAdditionalInfoDto>()
                .ForMember(dest => dest.Total,
                            opt => opt.MapFrom(src => Math.Round(src.UnitCost * src.Quantity)))
                .ForMember(dest => dest.PricePerUnit,
                            opt => opt.MapFrom(src => Math.Round(src.PricePerUnit, 2)));

            CreateMap<OrderDetails, OrderAdditionalInfoDto>()
                .ForMember(dest => dest.Address,
                            opt => opt.MapFrom(src => src.CustomerInfo.Address.Address1))
                .ForMember(dest => dest.Country,
                            opt => opt.MapFrom(src => src.CustomerInfo.Address.Country))
                .ForMember(dest => dest.EmailAddress,
                            opt => opt.MapFrom(src => src.CustomerInfo.Address.EmailAddress))
                .ForMember(dest => dest.FullName,
                            opt => opt.MapFrom(src => src.CustomerInfo.Address.FullName))
                .ForMember(dest => dest.PhoneNumber,
                            opt => opt.MapFrom(src => src.CustomerInfo.Address.PhoneNumber))
                .ForMember(dest => dest.Town,
                            opt => opt.MapFrom(src => src.CustomerInfo.Address.Town))
                .ForMember(dest => dest.PostCode,
                            opt => opt.MapFrom(src => src.CustomerInfo.Address.PostCode))
                .ForMember(dest => dest.Country,
                            opt => opt.MapFrom(src => src.CustomerInfo.Address.Country))
                .ForMember(dest => dest.ShippingMethod,
                            opt => opt.MapFrom(src => src.ShippingInfo.PostalServiceName))
                .ForMember(dest => dest.Packaging,
                            opt => opt.MapFrom(src => src.ShippingInfo.PackageCategory))
                .ForMember(dest => dest.PackageType,
                            opt => opt.MapFrom(src => src.ShippingInfo.PackageType))
                .ForMember(dest => dest.TrackingNumber,
                            opt => opt.MapFrom(src => src.ShippingInfo.TrackingNumber))
                .ForMember(dest => dest.ItemWeight,
                            opt => opt.MapFrom(src => src.ShippingInfo.ItemWeight))
                .ForMember(dest => dest.TotalWeight,
                            opt => opt.MapFrom(src => src.ShippingInfo.TotalWeight))
                .ForMember(dest => dest.PostageCostExTax,
                            opt => opt.MapFrom(src => src.ShippingInfo.PostageCostExTax))
                .ForMember(dest => dest.Tax,
                            opt => opt.MapFrom(src => Math.Round(src.TotalsInfo.Tax, 2)))
                .ForMember(dest => dest.Currency,
                            opt => opt.MapFrom(src => src.TotalsInfo.Currency))
                .ForMember(dest => dest.Total,
                            opt => opt.MapFrom(src => Math.Round(src.TotalsInfo.TotalCharge, 2)))
                .ForMember(dest => dest.Items,
                            opt => opt.MapFrom(src => src.Items));
        }
    }
}
