﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace TestApp.Helpers
{
    public static class GeneralHelpers
    {
        public static NumberFormatInfo NUM_FORMAT = new() { NumberGroupSeparator = "." };

        public static IEnumerable<T> Map<T>(List<Dictionary<String, Object>> data) where T : class, new()
        {
            var properties = typeof(T).GetProperties();
            var list = new List<T>();

            foreach(var item in data)
            {
                var instance = new T();

                foreach (var prop in properties)
                {
                    if(item.ContainsKey(prop.Name))
                    {
                        var propType = prop.PropertyType;
                        var value = item[prop.Name];
                        var instanceProperty = instance.GetType().GetProperty(prop.Name);

                        if (value is not null and not "")
                        {
                            instanceProperty.SetValue(instance, Convert.ChangeType(value, Type.GetType(propType.FullName), NUM_FORMAT));
                        }
                    }
                }

                list.Add(instance);
            }

            return list;
        }

        public static string ConvertUrlInBase64String(string url)
        {
            using WebClient client = new();

            var bytes = client.DownloadData(url);
            string base64string = Convert.ToBase64String(bytes);

            return base64string;
        }
    }
}
