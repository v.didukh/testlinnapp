﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestApp.Models.Common;

namespace TestApp.Helpers
{
    public class LimitRequestsHelper
    {
        private Dictionary<string, LastCallInfo> _callsInfo = new Dictionary<string, LastCallInfo>();
        private readonly TimeSpan _requestsDelay = TimeSpan.FromMinutes(1);

        public async Task IncrementRequests(string methodName, int maxRequests)
        {
            if (!_callsInfo.ContainsKey(methodName))
            {
                _callsInfo[methodName] = new LastCallInfo();
            }
            var callsInfo = _callsInfo[methodName];
            if (callsInfo.SentRequestsCount <= 0)
            {
                callsInfo.FirstRequestTime = DateTime.Now;
            }
            callsInfo.SentRequestsCount += 1;
            if (callsInfo.SentRequestsCount >= maxRequests)
            {
                var now = DateTime.Now;
                var timeDelta = now - callsInfo.FirstRequestTime;
                if (timeDelta < _requestsDelay)
                {
                    await Task.Delay(_requestsDelay - timeDelta);
                }
                callsInfo.SentRequestsCount = 0;
            }
        }
    }
}
