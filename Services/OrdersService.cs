﻿using LinnworksAPI;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TestApp.Common.Enums;
using TestApp.Helpers;
using TestApp.Models.Common;
using TestApp.Models.Dtos;
using TestApp.Models.Requests;
using TestApp.Models.Responses;

namespace TestApp.Services
{
    public class OrdersService : BaseService
    {
        private readonly LimitRequestsHelper _limitRequestsHelper; 
        public OrdersService(string sessionToken, string apiUrl) :
            base(sessionToken, apiUrl)
        {
            _limitRequestsHelper = new LimitRequestsHelper();
        }

        public async Task<QuickOrderResponse> CreateQuickOrder(QuickOrderRequest quickOrder)
        {
            var locationID = await Task.Run(() => ApiManager.Orders.GetUserLocationId());

            var locationData = await Task.Run(() => ApiManager.Locations.GetLocation(locationID));

            var countryName = locationData.Country;

            string scriptCountry = "Select pkCountryID As CountryID, cContinent As Continent " +
                                    "From ListCountries " +
                                    "WHERE cCountry LIKE '%" + countryName + "%'";

            var scriptCountryResult = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new LinnworksAPI.ExecuteCustomScriptQueryRequest()
            {
                Script = scriptCountry.ToString()
            }));

            var country = new CountryModel()
            {
                CountryID = Guid.Parse((string)(scriptCountryResult.Results.First()["CountryID"])),
                Continent = (string)(scriptCountryResult.Results.First()["CountryID"])
            };

            var fulfilmentCenter = new Guid("00000000-0000-0000-0000-000000000000");

            var newOrder = await Task.Run(() => ApiManager.Orders.CreateNewOrder(fulfilmentCenter, false));

            newOrder.GeneralInfo.SubSource = $"{quickOrder.Location} Braincence";

            await Task.Run(() => ApiManager.Orders.SetOrderGeneralInfo(newOrder.OrderId, newOrder.GeneralInfo, false));

            var orderCustomerInfo = new OrderCustomerInfo();
            orderCustomerInfo.ChannelBuyerName = quickOrder.CustomerName;

            orderCustomerInfo.BillingAddress = new CustomerAddress();
            orderCustomerInfo.BillingAddress.Company = quickOrder.Company;
            orderCustomerInfo.BillingAddress.EmailAddress = quickOrder.Email;
            orderCustomerInfo.BillingAddress.FullName = quickOrder.CustomerName;
            orderCustomerInfo.BillingAddress.PhoneNumber = quickOrder.PhoneNumber.ToString();
            orderCustomerInfo.BillingAddress.PostCode = quickOrder.PostCode;

            orderCustomerInfo.BillingAddress.Country = countryName;
            orderCustomerInfo.BillingAddress.CountryId = country != null ? country.CountryID : fulfilmentCenter;

            orderCustomerInfo.BillingAddress.Continent = country != null ? country.Continent : "";
            orderCustomerInfo.BillingAddress.Town = locationData.City;
            orderCustomerInfo.BillingAddress.Region = "";

            orderCustomerInfo.BillingAddress.Address1 = quickOrder.Address;
            orderCustomerInfo.BillingAddress.Address2 = "";
            orderCustomerInfo.BillingAddress.Address3 = "";

            orderCustomerInfo.Address = orderCustomerInfo.BillingAddress;

            var resultCustomer = await Task.Run(() => ApiManager.Orders.SetOrderCustomerInfo(newOrder.OrderId, orderCustomerInfo, false));

            foreach (var item in quickOrder.Items)
            {
                if (item.IsService)
                {
                    await Task.Run(() => ApiManager.Orders.AddOrderService(newOrder.OrderId, item.ItemTitle, item.UnitCost, item.TaxRate, fulfilmentCenter));
                }
                else
                {
                    var linePricing = new LinePricingRequest();
                    linePricing.PricePerUnit = item.UnitCost;
                    linePricing.TaxRatePercentage = item.TaxRate;


                    await Task.Run(() => ApiManager.Orders.AddOrderItem(newOrder.OrderId, Guid.Parse(item.StockItemId), item.SKU, fulfilmentCenter, item.Quantity, linePricing));
                }
            }

            if (quickOrder.Notes != null && quickOrder.Notes.Count > 0)
            {
                List<OrderNote> notes = new();
                foreach (var note in quickOrder.Notes)
                {
                    notes.Add(new OrderNote() {
                        OrderNoteId = new Guid(),
                        CreatedBy = "",
                        Internal = note.Internal,
                        Note = note.NoteText,
                        NoteDate = DateTime.ParseExact(note.Date, "dd/MM/yyyy hh:mm:ss", null),
                        OrderId = newOrder.OrderId });
                }

                await Task.Run(() => ApiManager.Orders.SetOrderNotes(newOrder.OrderId, notes));
            }

            int status;
            switch (quickOrder.Status)
            {
                case "Unpaid":
                    {
                        status = 0;
                        break;
                    }
                case "Paid":
                    {
                        status = 1;
                        break;
                    }
                case "Return":
                    {
                        status = 2;
                        break;
                    }
                case "Pending":
                    {
                        status = 3;
                        break;
                    }
                case "Resend":
                    {
                        status = 4;
                        break;
                    }
                default:
                    throw new Exception("There is no one matched statuses");

            }

            await Task.Run(() => ApiManager.Orders.ChangeStatus(new List<Guid>() { newOrder.OrderId }, status));

            var response = new QuickOrderResponse();

            if (quickOrder.RunRulesEngine)
            {
                await Task.Run(() => ApiManager.Orders.RunRulesEngine(new Guid[] { newOrder.OrderId }, null));
                response.IsRanRulesEngine = true;
            }

            if (quickOrder.AutoProcessOrder)
            {
                var processResult = await Task.Run(() => ApiManager.Orders.ProcessOrder(newOrder.OrderId, false, locationID));
                
                // Change it later
                if (string.IsNullOrWhiteSpace(processResult.Error))
                {
                    response.IsProcessed = processResult.Processed;
                }
                else
                {
                    throw new Exception($"There was an error while order was in the process. Message -> {processResult.Error}");
                }
            }

            if (quickOrder.PrintInvoice)
            {
                var invoiceResult = await PrintInvoice(newOrder.OrderId);

                response.InvoiceInBase64 = invoiceResult.InvoiceInBase64;
            }

            response.OrderNumber = newOrder.NumOrderId;

            return response;
        }

        public async Task<List<FilteredOrderDto>> GetFilteredOrders(string location, 
            string source, 
            string subSource, 
            string country,
            string dateFrom, 
            string dateTo, 
            StatusTypes status, 
            FilterTypes filterBy, 
            string itemId, 
            bool processed,
            int ordersAmount)
        {
            var ordersScript = @$"
                select
                    DISTINCT(o.pkOrderId) 'OrderId',
                    o.nOrderId 'OrderNumber',
                    o.dReceievedDate 'ReceivedDate',
                    CAST(o.fTotalCharge AS DECIMAL(10,2)) 'LineTotal',
                    o.cFullName 'Customer',
                    o.cCurrency 'Currency',
                    o.cEmailAddress 'Email',
                    o.BuyerPhoneNumber 'Phone',
                    o.Address1 'Address',
                    o.bProcessed 'IsProcessed',
                    o.Town 'City',
                    lc.cCountry 'Country'
                from [order] o 
                    inner join [orderitem] oi on oi.fkOrderID = o.pkOrderID
                    inner join [StockItems] sims on sims.pkStockID = oi.fkStockID
                    inner join [StockItem] si on si.pkStockItemID = sims.fkStockControlStockItemId 
                    inner join [ListCountries] lc on lc.pkCountryId = o.fkCountryId
                    inner join [StockLocation] sl on sl.pkStockLocationId = o.FulfillmentLocationId
                where ({filterBy} BETWEEN CONCAT(CONVERT(DATE, '{dateFrom}', 105), ' 00:00:00') AND CONCAT(CONVERT(DATE, '{dateTo}', 105), ' 23:59:59')) 
                and o.Source LIKE '{source}'
                and o.SubSource LIKE '{subSource}%'
                and lc.cCountry LIKE '{country}%'
                and o.nStatus = {(int)status}
                and si.pkStockItemID LIKE '{itemId}%'
                and sl.Location LIKE '{location}%'
            ";

            ordersScript = processed ? ordersScript + $" and o.bProcessed = {Convert.ToInt32(processed)}" : ordersScript;

            var filteredOrdersResponse = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new ExecuteCustomScriptQueryRequest
            {
                Script = ordersScript
            }));

            var filteredOrdersToMap = filteredOrdersResponse.Results.Take(ordersAmount).ToList();

            var mappedOrders = GeneralHelpers.Map<FilteredOrderDto>(filteredOrdersToMap);

            var itemsScript = @"
                select si.pkStockItemId 'ItemId', si.itemNumber 'SKU', oi.nQty 'Qty', (sl.Quantity - sl.InOrderBook) 'Available' from [order] o 
                    inner join [orderitem] oi on oi.fkOrderID = o.pkOrderID
                    inner join [StockItems] sims on sims.pkStockID = oi.fkStockID
                    inner join [StockItem] si on si.pkStockItemID = sims.fkStockControlStockItemId
                    inner join [StockLevel] sl on sl.fkStockItemId = si.pkStockItemId and sl.fkStockLocationId = oi.fkLocationId
                    --inner join [StockLocation] sloc on sloc.pkStockLocationId = oi.fkLocationId
                where o.pkOrderId = '{0}' --and sloc.location = '{1}'
            ";

            foreach (var order in mappedOrders)
            {
                await _limitRequestsHelper.IncrementRequests(nameof(ApiManager.Dashboards.ExecuteCustomScriptQuery), 250);
                var itemsResponse = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new ExecuteCustomScriptQueryRequest
                {
                    Script = string.Format(itemsScript, order.OrderId, location)
                }));

                var mappedItems = GeneralHelpers.Map<FilteredOrderItemDto>(itemsResponse.Results);

                order.Items = mappedItems as List<FilteredOrderItemDto>;
            }

            return mappedOrders as List<FilteredOrderDto>;
        }

        public async Task<CreatedPurchasesPesponse> CreatePurchaseOrder(Guid orderId, Guid[] itemsIds)
        {
            // GETTING CURRENT EXTENDED PROPERTIES OF ORDER
            var extendedProperties = await Task.Run(() => ApiManager.Orders.GetExtendedProperties(orderId));

            if(extendedProperties.Exists(x => x.Name.Equals("PURCHASE")))
            {
                return new CreatedPurchasesPesponse()
                {
                    AlreadyPurchased = true
                };
            }

            var itemsScript = $@"
                select si.pkStockItemId 'StockItemId', o.nOrderId 'OrderNumber', oi.nQty 'Quantity', oi.Cost, si.TaxRate, si.ItemNumber, oi.fkLocationId 'LocationId', o.cCurrency 'Currency' from [order] o
                    inner join [orderitem] oi on oi.fkOrderID = o.pkOrderID
                    inner join [stockitems] sims on sims.pkStockID = oi.fkStockID
                    inner join [stockitem] si on si.pkStockItemID = sims.fkStockControlStockItemId
                where o.pkOrderId = '{orderId}' and si.pkStockItemId IN ({ string.Join(",", itemsIds.Select(x => $"'{x}'")) })
            ";

            var suppliersScript = @"
                select fkSupplierId 'SupplierId', SupplierCode, SupplierPackSize 'PackSize' from [itemsupplier] 
                where fkStockItemId = '{0}' and isDefault = 1
            ";

            var responseItems = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new ExecuteCustomScriptQueryRequest
            {
                Script = itemsScript
            }));

            var mappedItems = GeneralHelpers.Map<Purchase>(responseItems.Results) as List<Purchase>;

            List<string> createdPoNumbers = new();
            Dictionary<string, List<Purchase>> itemsStorageBySupp = new();


            foreach (var item in mappedItems)
            {
                var responseSupplier = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new ExecuteCustomScriptQueryRequest
                {
                    Script = string.Format(suppliersScript, item.StockItemId)
                }));

                var mappedSupplier = GeneralHelpers.Map<Models.Common.Supplier>(responseSupplier.Results);

                var currentDefaultSupplier = mappedSupplier.FirstOrDefault();

                if (currentDefaultSupplier is not null)
                {
                    item.Supplier = currentDefaultSupplier;

                    var currentItemSuppId = currentDefaultSupplier.SupplierId.ToString();

                    var value = itemsStorageBySupp.TryGetValue(currentItemSuppId, out List<Purchase> items);

                    if (value)
                    {
                        items.Add(item);
                    }
                    else
                    {
                        itemsStorageBySupp.Add(currentItemSuppId, new List<Purchase> { item });
                    }
                }
            }

            if(itemsStorageBySupp.Any())
            {
                foreach(var itemsWithSimilarSupp in itemsStorageBySupp)
                {
                    var firstItem = itemsWithSimilarSupp.Value.First();
                    var externalInvoiceNumber = await GenerateExternalInvoiceNumber();
                    var purchaseId = await CreatePOWithExtendedProp(firstItem, externalInvoiceNumber.ToString());

                    var purchaseOrderResponse = await Task.Run(() => ApiManager.PurchaseOrder.Modify_PurchaseOrderItems_Bulk(new Modify_PurchaseOrderItems_BulkRequest
                    {
                        PurchaseId = purchaseId,
                        ItemsToAdd = itemsWithSimilarSupp.Value.Select(x =>
                        {
                            var purchasetem = new AddPurchaseOrderItem()
                            {
                                Id = Guid.NewGuid(),
                                StockItemId = Guid.Parse(x.StockItemId),
                                Qty = x.Quantity,
                                PackQuantity = 1,
                                PackSize = x.Supplier != null ? (x.Supplier.PackSize > 0 ? x.Supplier.PackSize : 1): 1,
                                Cost = x.Cost,
                                TaxRate = x.TaxRate >= 0 ? x.TaxRate : 0
                            };

                            mappedItems.Remove(x);

                            return purchasetem;
                        }).ToList(),
                        ItemsToDelete = new List<Guid>(),
                        ItemsToUpdate = new List<UpdatePurchaseOrderItem>()
                    }));

                    createdPoNumbers.Add(purchaseOrderResponse.PurchaseOrderHeader.ExternalInvoiceNumber);
                }
            }

            // CREATION PO FOR THE REST ORDERS
            foreach(var item in mappedItems)
            {
                var externalInvoiceNumber = await GenerateExternalInvoiceNumber();
                var purchaseId = await CreatePOWithExtendedProp(item, externalInvoiceNumber.ToString());

                var purchaseOrderResponse = await Task.Run(() => ApiManager.PurchaseOrder.Add_PurchaseOrderItem(new Add_PurchaseOrderItemParameter
                {
                    pkPurchaseId = purchaseId,
                    fkStockItemId = Guid.Parse(item.StockItemId),
                    Qty = item.Quantity,
                    PackQuantity = 1,
                    PackSize = item.Supplier != null ? (item.Supplier.PackSize > 0 ? item.Supplier.PackSize : 1) : 1,
                    Cost = item.Cost,
                    TaxRate = item.TaxRate >= 0 ? item.TaxRate: 0
                }));

                createdPoNumbers.Add(purchaseOrderResponse.PurchaseOrderHeader.ExternalInvoiceNumber);
            }

            // SETTING A NEW ONE WITH PURCHASE  
            extendedProperties.Add(new ExtendedProperty()
            {
                Name = "PURCHASE",
                Value = String.Join(",", createdPoNumbers),
                Type = "PURCHASE"
            });

            // SETTING UPDATED EXTENDED PROP TO CURRENT ORDER
            await Task.Run(() => ApiManager.Orders.SetExtendedProperties(orderId, extendedProperties.ToArray()));

            return new CreatedPurchasesPesponse()
            {
                PurchaseNumbers = createdPoNumbers
            };
        }

        public CsvFileResult GenerateOrdersCsvFile(OrdersRequest ordersRequest)
        {
            using MemoryStream fs = new();

            IWorkbook workbook = new XSSFWorkbook();
            ISheet sheet = workbook.CreateSheet($"{ordersRequest.DateFrom:dd/MM/yyyy} - {ordersRequest.DateTo:dd/MM/yyyy}");

            IRow row = sheet.CreateRow(0);
            var cells = typeof(OrderRequest).GetProperties();
            for (int i = 0; i < cells.Length; i++) 
            {
                row.CreateCell(i).SetCellValue(cells[i].Name);
            }

            for (int i = 0; i < ordersRequest.Orders.Count; i++)
            {
                IRow dataRow = sheet.CreateRow(i + 1);

                for (int j = 0; j < cells.Length; j++)
                {
                    var cellValue = cells[j].GetValue(ordersRequest.Orders[i]) ?? "";
                        
                    if(cells[j].Name.Equals("Items"))
                    {
                        var items = (List<OrderItemRequest>)cellValue;
                        dataRow.CreateCell(j).SetCellValue($"{String.Join(" / ", items.Select(x => x.SKU))}");
                    }
                    else
                    {
                        dataRow.CreateCell(j).SetCellValue(cellValue.ToString());
                    }
                }
            }

            workbook.Write(fs);

            return new CsvFileResult
            {
                BufferArray = fs.ToArray(),
                FileName = $"Orders ({ordersRequest.DateFrom:dd/MM/yyyy} - {ordersRequest.DateTo:dd/MM/yyyy}).xlsx"
            };
        }

        public async Task<OrderDetails> GetAdditionalOrderInfo(Guid orderId)
        {
            var orderInfo = await Task.Run(() => ApiManager.Orders.GetOrderById(orderId));
            return orderInfo;
        }

        public async Task<List<StockItemImage>> GetItemImages(Guid itemId)
        {
            var images = await Task.Run(() => ApiManager.Inventory.GetInventoryItemImages(itemId));
            return images;
        }

        public async Task<ProcessOrderResult> ProcessOrder(Guid orderId)
        {
            var locationID = await Task.Run(() => ApiManager.Orders.GetUserLocationId());

            var processResult = await Task.Run(() => ApiManager.Orders.ProcessOrder(orderId, false, locationID));

            return processResult;
        }

        public async Task<InvoiceResult> PrintInvoice(Guid orderId)
        {
            var fulfilmentLocationId = new Guid("00000000-0000-0000-0000-000000000000");

            var invoiceResponse = await Task.Run(() =>
                ApiManager.PrintService.CreatePDFfromJobForceTemplate("Invoice Template",
                    new List<Guid> { orderId },
                    null,
                    new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("LocationId", fulfilmentLocationId.ToString()) },
                    "PDF"));

            using WebClient client = new();

            var bytes = client.DownloadData(invoiceResponse.URL);
            string invoiceInBase64 = Convert.ToBase64String(bytes);

            if (invoiceResponse.KeyedError.Any() || invoiceResponse.PrintErrors.Any())
                throw new Exception($"There were errors while creating the invoice. Messages -> " +
                       $"{string.Join(", ", invoiceResponse.KeyedError.Select(x => x.Error))}; {string.Join(", ", invoiceResponse.PrintErrors)}");

            return new InvoiceResult() {
                InvoiceInBase64 = invoiceInBase64 
            };
        }

        public async Task<ShippingLabelResult> PrintShippingLabel(Guid orderId)
        {
            var shippingLabelResult = await Task.Run(() => ApiManager.PrintService.CreatePDFfromJobForceTemplate("Shipping Labels", new List<Guid>() { orderId }, null, null, "PDF", null));

            using WebClient client = new();

            var bytes = client.DownloadData(shippingLabelResult.URL);
            string shippingLabelInBase64 = Convert.ToBase64String(bytes);

            if (shippingLabelResult.KeyedError.Any() || shippingLabelResult.PrintErrors.Any())
                throw new Exception($"There were errors while printing the shipping label. Messages -> " +
                       $"{string.Join(", ", shippingLabelResult.KeyedError.Select(x => x.Error))}; {string.Join(", ", shippingLabelResult.PrintErrors)}");

            return new ShippingLabelResult()
            {
                ShippingLabelInBase64 = shippingLabelInBase64
            };
        }


        private static async Task<int> GenerateExternalInvoiceNumber()
        {
            var script = @"SELECT MAX(CAST(p.ExternalInvoicenumber AS INT)) As Reference
                            From Purchase p
                            LEFT JOIN purchase.Extended_Property isTest on isTest.fkPurchaseId = p.pkPurchaseId and isTest.PropertyName = 'ISTEST'
                        WHERE ISNUMERIC(p.ExternalInvoicenumber) = 1 AND p.ExternalInvoicenumber NOT LIKE '8888' and isTest.PropertyValue is null";

            var result = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new ExecuteCustomScriptQueryRequest
            {
                Script = script
            }));

            var number = int.Parse((string)result.Results.First()["Reference"]);

            number += 1;

            return number;
        }

        private static async Task<Guid> CreatePOWithExtendedProp(Purchase item, string externalInvoiceNumber)
        {
            var quotedDays = 2;

            var initPurchase = new Create_PurchaseOrder_InitialParameter
            {
                fkLocationId = Guid.Parse(item.LocationId),
                fkSupplierId = item.Supplier != null ? Guid.Parse(item.Supplier.SupplierId) : Guid.Parse("00000000-0000-0000-0000-000000000000"),
                DateOfPurchase = DateTime.UtcNow,
                QuotedDeliveryDate = DateTime.UtcNow.AddDays(quotedDays),
                ExternalInvoiceNumber = externalInvoiceNumber,
                Currency = item.Currency,
                SupplierReferenceNumber = item.Supplier?.SupplierCode ?? "Default",
                UnitAmountTaxIncludedType = 2,
                ConversionRate = 0
            };

            var purchaseId = await Task.Run(() => ApiManager.PurchaseOrder.Create_PurchaseOrder_Initial(initPurchase));

            // ADD EXTENDED PROP TO PURCHASE
            await Task.Run(() => ApiManager.PurchaseOrder.Add_PurchaseOrderExtendedProperty(new Add_PurchaseOrderExtendedPropertyRequest()
            {
                PurchaseId = purchaseId,
                ExtendedPropertyItems = new List<Add_PurchaseOrderExtendedProperty_Item>()
                {
                    new Add_PurchaseOrderExtendedProperty_Item()
                    {
                        PropertyName = "ORDER",
                        PropertyValue = item.OrderNumber
                    }
                }
            }));

            return purchaseId;
        }
    }
}
