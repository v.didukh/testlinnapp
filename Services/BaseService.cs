﻿using LinnworksAPI;
using System;

namespace TestApp.Services
{
    public abstract class BaseService
    {
        protected static LinnworksAPI.ApiObjectManager ApiManager { get; set; }

        public BaseService(string sessionToken, string apiUrl)
        {
            ApiManager = new LinnworksAPI.ApiObjectManager(new ApiContext(Guid.Parse(sessionToken), apiUrl));
        }
    }
}
