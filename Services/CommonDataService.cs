﻿using LinnworksAPI;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Helpers;
using TestApp.Models.Common;

namespace TestApp.Services
{
    public class CommonDataService : BaseService
    {
        public CommonDataService(string sessionToken, string apiUrl) : 
            base(sessionToken, apiUrl)
        {
        }

        public async Task<List<OrderFolder>> GetFolders()
        {
            var folders = await Task.Run(() => ApiManager.Orders.GetAvailableFolders());
            return folders;
        }

        public async Task<List<Category>> GetCategories()
        {
            var categories = await Task.Run(() => ApiManager.Inventory.GetCategories());
            return categories;
        }

        public async Task<List<InventoryStockLocation>> GetStockLocations()
        {
            var stockLocations = await Task.Run(() => ApiManager.Inventory.GetStockLocations());
            return stockLocations;
        }

        public async Task<List<LinnworksAPI.Supplier>> GetSuppliers()
        {
            var suppliers = await Task.Run(() => ApiManager.Inventory.GetSuppliers());
            return suppliers;
        }

        public async Task<List<Dictionary<string, object>>> GetMatchedStockItemsNumber(string match)
        {
            var script = new ExecuteCustomScriptQueryRequest
            {
                Script = $@"
                    SELECT TOP 20 pkStockItemId, ItemNumber, BarcodeNumber FROM StockItem 
                    WHERE (ItemNumber LIKE '{match}%' OR BarcodeNumber LIKE '{match}%') AND BarcodeNumber != ''"
            };

            var stockItems = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(script));

            return stockItems.Results;
        }

        public async Task<Dictionary<string, object>> GetStockItemBySKU(string stockItemId, string location)
        {
            var script = new ExecuteCustomScriptQueryRequest
            {
                Script = $@"
                select pkStockItemID 'StockItemId', ItemNumber 'SKU', ItemTitle, ROUND(RetailPrice, 2) 'UnitCost', TaxRate 'TaxRate', (Quantity - InOrderBook) 'Available' from stockItem 
                inner join stocklevel on fkStockItemId = pkStockItemId
                inner join stocklocation on pkStockLocationId = fkStockLocationId
                where pkStockItemID = '{stockItemId}' and location = '{location}'"
            };

            var stockItems = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(script));

            return stockItems.Results.FirstOrDefault();
        }

        public async Task<List<Source>> GetSources()
        {
            var sources = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new ExecuteCustomScriptQueryRequest
            {
                Script = "select DISTINCT(source) 'Name' from [order]"
            }));

            var mappedSources = GeneralHelpers.Map<Source>(sources.Results);

            return mappedSources as List<Source>;
        }
    }
}
