﻿using LinnworksAPI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Dto;
using TestApp.Helpers;
using TestApp.Models;

namespace TestApp.Services
{
    public class ReportService : BaseService
    {
        public ReportService(string sessionToken, string apiUrl) :
            base(sessionToken, apiUrl)
        {

        }

        /// <summary>
        /// Report, which represents the data about Italy paid orders, 
        /// which aren't hold in Ukraine folder (1.1)
        /// </summary>
        public async Task<IEnumerable<ItalyPaidOrder_1_1_Model>> GetPaidItalyOrdersReport(string folder)
        {
            var query = new ExecuteCustomScriptQueryRequest
            {
                Script = $@"Select o.nOrderId OrderNumber, o.CreatedDate, o.cCurrency Currency From [Open_Order] o
                                LEFT JOIN Order_folder folder ON folder.fkOrderID = o.pkOrderID AND folder.FolderName = '{folder}'
                                inner join ListCountries country on country.pkCountryID = o.fkCountryID and country.cCountry = 'Italy'
                            Where o.nStatus = 1 and folder.foldername is null;"
            };

            var response = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(query));

            var mapped = GeneralHelpers.Map<ItalyPaidOrder_1_1_Model>(response.Results);

            return mapped;
        }

        /// <summary>
        /// Report, which represents the data about orders, 
        /// which will be arrived today (1.3)
        /// </summary>
        public async Task<IEnumerable<PurchaseOrder_1_3_Model>> PurchaseOrdersWhichWillBeArrivedTodayReport()
        {
            var query = @"
                DECLARE @startdate DATETIME;
                SET @startdate = DATETIMEFROMPARTS(YEAR(GETDATE()), MONTH(GETDATE()), DAY(GETDATE()), 00, 00, 00, 00);
                DECLARE @enddate DATETIME;
                SET @enddate = DATETIMEFROMPARTS(YEAR(GETDATE()), MONTH(GETDATE()), DAY(GETDATE()), 23, 59, 59, 59);
                SELECT
                    p.ExternalInvoiceNumber as Reference,
                    sLocation.Location,
                    sLocation.Address1,
                    sLocation.City as Town,
                    sLocation.Country,
                    s.SupplierName,
                    p.SupplierReferenceNumber AS SupplierRef,
                    p.Currency,
                    sItem.ItemNumber AS SKU,
                    sItem.ItemTitle AS Title,
                    iSupplier.SupplierCode,
                    iSupplier.SupplierBarcode,
                    pItem.Quantity,
                    iSupplier.KnownPurchasePrice AS UnitCost,
                    pItem.Tax,
                    pItem.Cost
                FROM [Purchase] p
                    INNER JOIN [StockLocation] as sLocation ON sLocation.pkStockLocationId = p.fkLocationId
                    INNER JOIN [Supplier] as s ON s.pkSupplierID = p.fkSupplierId
                    INNER JOIN [PurchaseItem] as pItem ON pItem.fkPurchasId = p.pkPurchaseId
                    INNER JOIN [StockItem] as sItem ON sItem.pkStockItemId = pItem.fkStockItemId
                    LEFT JOIN [ItemSupplier] iSupplier ON iSupplier.fkStockItemId = sItem.pkStockItemId
                WHERE 
                    (p.QuotedDeliveryDate BETWEEN @startdate AND @enddate)
                AND 
                    (iSupplier.IsDefault = 1 OR iSupplier.IsDefault IS NULL)
                GROUP BY
                    p.pkPurchaseId,
                    p.ExternalInvoiceNumber,
                    sLocation.Location,
                    sLocation.Address1,
                    sLocation.City,
                    sLocation.Country,
                    s.SupplierName,
                    p.SupplierReferenceNumber,
                    p.Currency,
                    sItem.ItemNumber,
                    sItem.ItemTitle,
                    iSupplier.SupplierCode,
                    iSupplier.SupplierBarcode,
                    pItem.Quantity,
                    iSupplier.KnownPurchasePrice,
                    pItem.Tax,
                    pItem.Cost,
                    pItem.SortOrder
                ORDER BY pItem.SortOrder DESC
            ";

            var queryInstance = new LinnworksAPI.ExecuteCustomScriptQueryRequest
            {
                Script = query
            };

            var response = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(queryInstance));

            var mapped = GeneralHelpers.Map<PurchaseOrder_1_3_Model>(response.Results);

            return mapped;
        }

        /// <summary>
        /// Report, which represents the data about Star Wars items (1.6)
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<StartWarsItem_1_6_Model>> GetStartWarsItemsReport(StarWarsItemsReportRequest request)
        {

            var query = new ExecuteCustomScriptQueryRequest
            {
                Script = $@"
                    DECLARE @StartDate datetime = CONVERT(DATETIME, '{request.StartDate}', 105);

                    WITH OrdersUA AS 
                    (
                        SELECT si.pkStockItemID AS StockItemID, 
                                SUM(oi.fPricePerUnit * oi.nQty) AS SalesUA, 
                                SUM(oi.nQty) AS SoldUA
                        FROM [Order] o
                            INNER JOIN ListCountries country ON o.fkCountryID = country.pkCountryID AND country.cCountry = 'Ukraine'
                            INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                            INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                            INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                            INNER JOIN ProductCategories cat ON cat.CategoryID = si.CategoryId
                        WHERE o.dProcessedOn Between @StartDate AND GETDATE() 
                            AND (cat.CategoryName = 'Star Wars')
                        GROUP BY si.pkStockItemID
                    ), 
                    OrdersIT AS (
                        SELECT si.pkStockItemID AS StockItemID, 
                                SUM(oi.fPricePerUnit * oi.nQty) AS SalesIT, 
                                SUM(oi.nQty) AS SoldIT
                        FROM [Order] o
                            INNER JOIN ListCountries country ON o.fkCountryID = country.pkCountryID AND country.cCountry = 'Italy'
                            INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                            INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                            INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                            INNER JOIN ProductCategories cat ON cat.CategoryID = si.CategoryId
                        WHERE o.dProcessedOn Between @StartDate AND GETDATE() 
                            AND (cat.CategoryName = 'Star Wars')
                        GROUP BY si.pkStockItemID
                    ),
                    OrdersUK AS (
                        SELECT si.pkStockItemID AS StockItemID, 
                                SUM(oi.fPricePerUnit * oi.nQty) AS SalesUK, 
                                SUM(oi.nQty) AS SoldUK
                        FROM [Order] o
                            INNER JOIN ListCountries country ON o.fkCountryID = country.pkCountryID AND country.cCountry = 'United Kingdom'
                            INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                            INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                            INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                            INNER JOIN ProductCategories cat ON cat.CategoryID = si.CategoryId
                        WHERE o.dProcessedOn Between @StartDate AND GETDATE() 
                            AND (cat.CategoryName = 'Star Wars')
                        GROUP BY si.pkStockItemID
                    ),
                    OrdersPaypal AS (
                        SELECT si.pkStockItemID AS StockItemID, 
                                SUM(oi.fPricePerUnit * oi.nQty) AS SalesPaypal
                        FROM [Order] o
                            INNER JOIN Accounting_Bank abank ON abank.pkBankId = o.fkBankId
                            INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                            INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                            INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                            INNER JOIN ProductCategories cat ON cat.CategoryID = si.CategoryId
                        WHERE (o.dProcessedOn Between @StartDate AND GETDATE())
                            AND (cat.CategoryName = 'Star Wars')
                            AND (abank.AccountName = 'PayPal')
                        GROUP BY si.pkStockItemID
                    ),
                    OrdersCash AS (
                        SELECT si.pkStockItemID AS StockItemID, 
                                SUM(oi.fPricePerUnit * oi.nQty) AS SalesCash
                        FROM [Order] o
                        INNER JOIN Accounting_Bank abank ON abank.pkBankId = o.fkBankId
                        INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                        INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                        INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                        INNER JOIN ProductCategories cat ON cat.CategoryID = si.CategoryId
                        WHERE (o.dProcessedOn Between @StartDate AND GETDATE())
                            AND (cat.CategoryName = 'Star Wars')
                            AND (abank.AccountName = 'Default')
                        GROUP BY si.pkStockItemID
                    ),
                    OrdersTotal AS (
                        SELECT si.pkStockItemID AS StockItemID, 
                                SUM(oi.fPricePerUnit * oi.nQty) AS TotalSales, 
                                SUM(oi.nQty) AS TotalSold
                        FROM [Order] o
                            INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                            INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                            INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                            INNER JOIN ProductCategories cat ON cat.CategoryID = si.CategoryId
                        WHERE (o.dProcessedOn Between @StartDate AND GETDATE())
                        AND (cat.CategoryName = 'Star Wars')
                        GROUP BY si.pkStockItemID
                    ),
                    Orders_CTN_QTY AS (
                        SELECT si.pkStockItemID AS StockItemID, 
                                siExtended.ProperyValue AS CTN_QTY
                        FROM [StockItem] si
                            INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                        WHERE siExtended.ProperyName = 'CTN QTY'
                    ),
                    Orders_CBM AS (
                        SELECT si.pkStockItemID AS StockItemID, 
                                siExtended.ProperyValue AS CBM
                        FROM [StockItem] si
                            INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                        WHERE siExtended.ProperyName = 'CBM'
                    ),
                    TotalStockLevel AS (
                        SELECT si.pkStockItemID AS StockItemID,
                            SUM(sLevel.Quantity) AS TotalStockLevel
                        FROM [Order] o
                            INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                            INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                            INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                            INNER JOIN StockLevel sLevel ON sLevel.fkStockItemID = si.pkStockItemID AND sLevel.fkStockLocationId = oi.fkLocationId
                            GROUP BY si.pkStockItemID
                    )
                    SELECT 
                        si.ItemNumber AS SKU, 
                        oUA.*, 
                        oIT.*,
                        oUK.*,
                        oPaypal.*,
                        oCash.*,
                        oTotal.*,
                        oCTN.CTN_QTY,
                        oCBM.CBM AS CBM_PER_QTY,
                        (totalStock.TotalStockLevel * (oCBM.CBM / IIF(oCTN.CTN_QTY <= 0, 1, CAST(oCTN.CTN_QTY AS FLOAT)))) AS TotalCBM
                    FROM StockItem si
                        INNER JOIN ProductCategories pc ON pc.CategoryId = si.CategoryId
                        LEFT JOIN OrdersUA oUA ON si.pkStockItemID = oUA.StockItemID
                        LEFT JOIN OrdersIT oIT ON si.pkStockItemID = oIT.StockItemID
                        LEFT JOIN OrdersUK oUK ON si.pkStockItemID = oUK.StockItemID
                        LEFT JOIN OrdersPaypal oPaypal ON si.pkStockItemID = oPaypal.StockItemID
                        LEFT JOIN OrdersCash oCash ON si.pkStockItemID = oCash.StockItemID
                        LEFT JOIN OrdersTotal oTotal ON si.pkStockItemID = oTotal.StockItemID
                        LEFT JOIN Orders_CTN_QTY oCTN ON si.pkStockItemID = oCTN.StockItemID
                        LEFT JOIN Orders_CBM oCBM ON si.pkStockItemID = oCBM.StockItemID
                        LEFT JOIN TotalStockLevel totalStock ON si.pkStockItemID = totalStock.StockItemID
                    WHERE pc.CategoryName = 'Star Wars';"
            };

            var response = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(query));

            var mapped = GeneralHelpers.Map<StartWarsItem_1_6_Model>(response.Results);

            return mapped;
        }

        /// <summary>
        /// Report, which represents the data about Returns and refunds task (1.8)
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ReturnsRefunds_1_8_Model>> GetReturnsAndRefundsReport(ReturnsAndRefundsReportRequest request)
        {
            var script = $@"
                SELECT 
                    si.ItemNumber 'SKU', 
                    si.BarcodeNumber 'Barcode',
                    si.ItemTitle 'Description',
                    o.Source,
                    o.SubSource, 
                    o.cFullName 'FullName',
                    o.cEmailAddress 'Email',
                    o.cShippingAddress 'Address',
                    o.Town 'City',
                    o.cPostCode 'PostalCode',
                    o.ReferenceNum 'Reference',
                    o.ExternalReference,
                    o.CreatedDate 'OrderDate',
                    o.dProcessedOn 'ProcessDate',
                    refund.ActionDate 'ReturnDate',
                    refund.Amount 'RefundGiven',
                    o.cCurrency 'Currency',
                    refund.Reason
                FROM Order_Refund refund
                    INNER JOIN [Order] o ON o.pkOrderID = refund.fkOrderID
                    INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                    INNER JOIN StockItems sims ON sims.pkStockID = oi.fkStockID
                    INNER JOIN StockItem si ON si.pkStockItemID =  sims.fkStockControlStockItemId
                    INNER JOIN ListCountries lc ON lc.pkCountryId = o.fkCountryId
                WHERE refund.Actioned = 1 AND (refund.CreateDate BETWEEN CONVERT(DATETIME, '{request.DateFrom}', 105) AND CONVERT(DATETIME, '{request.DateTo}', 105))
                GROUP BY 
                    si.ItemNumber,
                    si.BarcodeNumber,
                    si.ItemDescription,
                    o.Source,
                    o.SubSource, 
                    o.cFullName,
                    o.cEmailAddress,
                    o.cShippingAddress,
                    o.Town,
                    o.cPostCode,
                    o.ReferenceNum,
                    o.ExternalReference,
                    o.CreatedDate,
                    o.dProcessedOn,
                    refund.ActionDate,
                    refund.Amount,
                    o.cCurrency,
                    refund.Reason";

            var response = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new LinnworksAPI.ExecuteCustomScriptQueryRequest
            {
                Script = script,
            }));

            var mapped = GeneralHelpers.Map<ReturnsRefunds_1_8_Model>(response.Results);

            return mapped;
        }

        /// <summary>
        /// Stock report (1.9)
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<StockReport_1_9_Model>> GetStockReport(StockReportRequest request)
        {
            var script = $@"
                WITH CTN_QTY AS (
                    SELECT 
                        si.pkStockItemID StockItemID, 
                        siExtended.ProperyValue CTN_QTY
                    FROM [StockItem] si
                        INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                    WHERE siExtended.ProperyName = 'CTN QTY'
                ),
                CBM AS (
                    SELECT 
                        si.pkStockItemID StockItemID, 
                        siExtended.ProperyValue CBM
                    FROM [StockItem] si
                        INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                    WHERE siExtended.ProperyName = 'CBM'
                ),
                TotalStockLevel AS (
                    SELECT si.pkStockItemID StockItemID,
                        SUM(sLevel.Quantity) TotalStockLevel
                    FROM [Order] o
                        INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                        INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                        INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                        INNER JOIN StockLevel sLevel ON sLevel.fkStockItemID = si.pkStockItemID AND sLevel.fkStockLocationId = oi.fkLocationId
                    GROUP BY si.pkStockItemID
                )
                SELECT 
                    sI.ItemNumber SKU, 
                    sI.ItemTitle Description, 
                    pC.CategoryName Category, 
                    sI.BarcodeNumber Barcode,
                    sI.RetailPrice,
                    sI.PurchasePrice,
                    sLevel.MinimumLevel MinLevel,
                    sLevel.Quantity StockLevel,
                    (sLevel.Quantity - sLevel.InOrderBook) Available,
                    sLevel.CurrentStockValue StockValue,
                    sI.Weight,
                    sI.DimHeight Height,
                    sI.DimDepth 'Depth',
                    x.CTN_QTY,
                    xx.CBM CBM_PER_QTY,
                    (totalStock.TotalStockLevel * (xx.CBM / IIF(x.CTN_QTY <= 0, 1, CAST(x.CTN_QTY AS FLOAT)))) AS TotalCBM
                FROM [StockItem] sI
                    INNER JOIN [ProductCategories] pC ON sI.CategoryId = pC.CategoryId
                    INNER JOIN [StockLevel] sLevel ON sLevel.fkStockItemId = sI.pkStockItemId
                    INNER JOIN [StockLocation] sL ON sL.pkStockLocationId = sLevel.fkStockLocationId
                    LEFT JOIN CTN_QTY x ON sI.pkStockItemID = x.StockItemID
                    LEFT JOIN CBM xx ON sI.pkStockItemID = xx.StockItemID
                    LEFT JOIN TotalStockLevel totalStock ON sI.pkStockItemID = totalStock.StockItemID
                WHERE sL.Location = '{request.Location}' AND pC.CategoryName = '{request.Category}';";

            var response = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(new LinnworksAPI.ExecuteCustomScriptQueryRequest
            {
                Script = script
            }));

            var mapped = GeneralHelpers.Map<StockReport_1_9_Model>(response.Results);

            return mapped;
        }

        /// <summary>
        /// Report, which represents Items, which have got a specific supplier as default (1.10)
        /// </summary>
        public async Task<IEnumerable<ItemsByDefaultSuppReport_1_10_Model>> GetItemsByDefaultSupplierReport(string supplier)
        {
            var query = new ExecuteCustomScriptQueryRequest
            {
                Script = $@"
                        WITH CTN_QTY AS (
                            SELECT 
                                si.pkStockItemID StockItemID, 
                                siExtended.ProperyValue CTN_QTY
                            FROM [StockItem] si
                                INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                            WHERE siExtended.ProperyName = 'CTN QTY'
                        ),
                        CBM AS (
                            SELECT 
                                si.pkStockItemID StockItemID, 
                                siExtended.ProperyValue CBM
                            FROM [StockItem] si
                                INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                            WHERE siExtended.ProperyName = 'CBM'
                        ),
                        CommodityCode  AS (
                            SELECT 
                                si.pkStockItemID StockItemID, 
                                siExtended.ProperyValue CommodityCode
                            FROM [StockItem] si
                                INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                            WHERE siExtended.ProperyName = 'CommodityCode'
                        )
                        SELECT 
                            sI.ItemNumber SKU, 
                            sI.ItemTitle Description, 
                            pC.CategoryName Category, 
                            iSup.KnownPurchasePrice KnownPurchasePrice,
                            sI.BarcodeNumber Barcode,
                            sI.Weight,
                            sI.DimHeight Height,
                            sI.DimDepth 'Depth',
                            x.CTN_QTY,
                            xx.CBM CBM_PER_QTY,
                            cC.CommodityCode
                        FROM [StockItem] sI
                            INNER JOIN [ProductCategories] pC ON sI.CategoryId = pC.CategoryId
                            INNER JOIN [ItemSupplier] iSup ON iSup.fkStockItemId = sI.pkStockItemId
                            INNER JOIN [Supplier] sup ON sup.pkSupplierID = iSup.fkSupplierId
                            LEFT JOIN CTN_QTY x ON sI.pkStockItemID = x.StockItemID
                            LEFT JOIN CBM xx ON sI.pkStockItemID = xx.StockItemID
                            LEFT JOIN CommodityCode cC ON sI.pkStockItemID = cC.StockItemID
                        WHERE sup.SupplierName = '{supplier}' AND iSup.IsDefault = 1;"
            };

            var response = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(query));

            var mapped = GeneralHelpers.Map<ItemsByDefaultSuppReport_1_10_Model>(response.Results);

            return mapped;
        }

        /// <summary>
        /// Report, which represents purchase orders, which will be delivered soon (1.11)
        /// </summary>
        public async Task<IEnumerable<PurchaseAsDeliveredSoonReport_1_11_Model>> GetPurchaseOrdersByItemAsDeliveredSoon(PurchaseOrdersByItemReportRequest request)
        {
            var query = new ExecuteCustomScriptQueryRequest
            {
                Script = $@"
                     WITH CTN_QTY AS (
                        SELECT 
                            si.pkStockItemID StockItemID, 
                            siExtended.ProperyValue CTN_QTY
                        FROM [StockItem] si
                            INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                        WHERE siExtended.ProperyName = 'CTN QTY'
                    ),
                    CBM AS (
                        SELECT 
                            si.pkStockItemID StockItemID, 
                            siExtended.ProperyValue CBM
                        FROM [StockItem] si
                            INNER JOIN StockItem_ExtendedProperties siExtended ON siExtended.fkStockItemId = si.pkStockItemId
                        WHERE siExtended.ProperyName = 'CBM'
                    ),
                     TotalStockLevel AS (
                        SELECT si.pkStockItemID StockItemID,
                            SUM(sLevel.Quantity) TotalStockLevel
                        FROM [Order] o
                            INNER JOIN OrderItem oi ON oi.fkOrderID = o.pkOrderID
                            INNER JOIN StockItems sis ON sis.pkStockID = oi.fkStockID
                            INNER JOIN StockItem si ON si.pkStockItemID = sis.fkStockControlStockItemId
                            INNER JOIN StockLevel sLevel ON sLevel.fkStockItemID = si.pkStockItemID AND sLevel.fkStockLocationId = oi.fkLocationId
                        GROUP BY si.pkStockItemID
                    )
                    SELECT 
                        pExtended.PropertyValue,
                        p.ExternalInvoiceNumber PONumber, 
                        p.QuotedDeliveryDate ExpectedDate,  
                        pI.Quantity Qty, 
                        pI.Cost,
                        x.CTN_QTY,
                        xx.CBM CBM_PER_QTY,
                        (totalStock.TotalStockLevel * (xx.CBM / IIF(x.CTN_QTY <= 0, 1, CAST(x.CTN_QTY AS FLOAT)))) TotalCBM
                    FROM Purchase p 
                        INNER JOIN PurchaseItem pI ON pI.fkPurchasId = p.pkPurchaseId
                        INNER JOIN purchase.extended_property pExtended ON pExtended.fkPurchaseId = p.pkPurchaseId
                        INNER JOIN StockItem sI ON sI.pkStockItemId = pI.fkStockItemID
                        LEFT JOIN CTN_QTY x ON sI.pkStockItemId = x.StockItemID
                        LEFT JOIN CBM xx ON sI.pkStockItemId = xx.StockItemID
                        LEFT JOIN TotalStockLevel totalStock ON sI.pkStockItemId = totalStock.StockItemID
                    WHERE (Status = 'PENDING' OR Status = 'OPEN') 
                        AND pExtended.PropertyName = 'GOODS READY DATE' 
                        AND (p.QuotedDeliveryDate <= DATEADD(dd, 2, DATETIMEFROMPARTS(YEAR(GETDATE()), MONTH(GETDATE()), DAY(GETDATE()), 23, 59, 59, 99)) AND p.QuotedDeliveryDate >= DATEADD(dd, 0, DATETIMEFROMPARTS(YEAR(GETDATE()), MONTH(GETDATE()), DAY(GETDATE()), 00, 00, 00, 00)))
                        AND sI.ItemNumber = '{request.itemSKU}'"
            };

            var response = await Task.Run(() => ApiManager.Dashboards.ExecuteCustomScriptQuery(query));

            var mapped = GeneralHelpers.Map<PurchaseAsDeliveredSoonReport_1_11_Model>(response.Results);

            return mapped;
        }
    }
}
