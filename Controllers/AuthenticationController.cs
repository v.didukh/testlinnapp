﻿using LinnworksAPI;
using Microsoft.AspNetCore.Mvc;
using System;
using TestApp.Common;
using TestApp.Dto;

namespace TestApp.Controllers
{
    [ApiController]
    [Route("api/auth")]
    public class AuthenticationController : ControllerBase
    {
        private readonly LinnworksSettings _settings;
        public AuthenticationController(LinnworksSettings settings)
        {
            _settings = settings;
        }

        [HttpPost]
        public ActionResult<UserCredentials> Authenticate()
        {
            try
            {
                var linnAuthController = new AuthController(new ApiContext(_settings.AuthApiUrl));

                var session = linnAuthController.AuthorizeByApplication(new AuthorizeByApplicationRequest
                {
                    ApplicationId = _settings.ApplicationId,
                    ApplicationSecret = _settings.ApplicationSecret,
                    Token = _settings.ApplicationToken
                });

                var userCredentials = new UserCredentials(session.UserId.ToString(), session.Token.ToString(), session.Email);

                return Ok(userCredentials);
            }
            catch (Exception)
            {
                return Unauthorized();
            }
        }

    }
}
