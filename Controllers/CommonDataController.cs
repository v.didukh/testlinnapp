﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Common;
using TestApp.Helpers;
using TestApp.Models.Common;
using TestApp.Services;

namespace TestApp.Controllers
{
    [Route("api/common")]
    [ApiController]
    public class CommonDataController : ControllerBase
    {
        private readonly ILogger<CommonDataController> _logger;
        private readonly LinnworksSettings _linnworksSettings;
        private readonly IMapper _mapper;

        public CommonDataController(ILogger<CommonDataController> logger, LinnworksSettings linnworksSettings, IMapper mapper)
        {
            _logger = logger;
            _linnworksSettings = linnworksSettings;
            _mapper = mapper;
        }

        [HttpGet("folders")]
        public async Task<ActionResult<List<OrderFolderDto>>> GetAllExistingFolders()
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var commonDataService = new CommonDataService(accessToken, _linnworksSettings.DataApiUrl);
                var folders = await commonDataService.GetFolders();
                var mappedFolders = _mapper.Map<List<OrderFolderDto>>(folders);

                return mappedFolders;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetAllExistingFolders)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("stockLocations")]
        public async Task<ActionResult<List<StockLocationDto>>> GetAllExistingStockLocations()
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var commonDataService = new CommonDataService(accessToken, _linnworksSettings.DataApiUrl);
                var stockLocations = await commonDataService.GetStockLocations();
                var mappedStockLocations = _mapper.Map<List<StockLocationDto>>(stockLocations);

                return mappedStockLocations;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetAllExistingStockLocations)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("suppliers")]
        public async Task<ActionResult<List<SupplierDto>>> GetAllExistingSuppliers()
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var commonDataService = new CommonDataService(accessToken, _linnworksSettings.DataApiUrl);
                var suppliers = await commonDataService.GetSuppliers();
                var mappedSuppliers = _mapper.Map<List<SupplierDto>>(suppliers);

                return mappedSuppliers;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetAllExistingSuppliers)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("categories")]
        public async Task<ActionResult<List<CategoryDto>>> GetAllExistingCategories()
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var commonDataService = new CommonDataService(accessToken, _linnworksSettings.DataApiUrl);
                var suppliers = await commonDataService.GetCategories();
                var mappedCategories = _mapper.Map<List<CategoryDto>>(suppliers);

                return mappedCategories;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetAllExistingSuppliers)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("matchedStockItems")]
        public async Task<ActionResult<List<StockItemPartialDto>>> GetAllMatchedStockItemsByNumber([FromQuery] string match = "")
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var commonDataService = new CommonDataService(accessToken, _linnworksSettings.DataApiUrl);
                var stockItems = await commonDataService.GetMatchedStockItemsNumber(match);
                var mappedStockItems = 
                    stockItems.Select(x => new StockItemPartialDto { 
                        StockItemId = (string)x["pkStockItemId"],
                        ItemInfo = $"{(string)x["ItemNumber"]} - {(string)x["BarcodeNumber"]}" 
                    })
                    .ToList();

                return mappedStockItems;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetAllMatchedStockItemsByNumber)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("stockItem")]
        public async Task<ActionResult<StockItemDto>> GetStockItemBySKU([FromQuery] string stockItemId, [FromQuery] string location)
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var commonDataService = new CommonDataService(accessToken, _linnworksSettings.DataApiUrl);
                var stockItem = await commonDataService.GetStockItemBySKU(stockItemId, location);

                if(stockItem != null)
                {
                    var mappedStockItem = new StockItemDto()
                    {
                        StockItemId = (string)stockItem["StockItemId"],
                        SKU = (string)stockItem["SKU"],
                        ItemTitle = (string)stockItem["ItemTitle"],
                        UnitCost = double.Parse((string)stockItem["UnitCost"], GeneralHelpers.NUM_FORMAT),
                        TaxRate = double.Parse((string)stockItem["TaxRate"]),
                        Available = int.Parse((string)stockItem["Available"])
                    };

                    return mappedStockItem;
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetStockItemBySKU)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("getSources")]
        public async Task<ActionResult<List<Source>>> GetSources()
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var commonDataService = new CommonDataService(accessToken, _linnworksSettings.DataApiUrl);
                var mappedSources = await commonDataService.GetSources();

                return mappedSources;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetSources)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }
    }
}
