﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestApp.Common;
using TestApp.Common.Enums;
using TestApp.Models.Dtos;
using TestApp.Models.Requests;
using TestApp.Models.Responses;
using TestApp.Services;
using AutoMapper;
using TestApp.Models.Common;
using LinnworksAPI;
using TestApp.ModelBinders;
using System.Linq;
using TestApp.Helpers;

namespace TestApp.Controllers
{
    [Route("api/orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ILogger<OrdersController> _logger;
        private readonly IMapper _mapper;
        private readonly LinnworksSettings _linnworksSettings;
        public OrdersController(ILogger<OrdersController> logger, LinnworksSettings linnworksSettings, IMapper mapper)
        {
            _logger = logger;
            _linnworksSettings = linnworksSettings;
            _mapper = mapper;
        }

        [HttpPost("createQuickOrder")]
        public async Task<ActionResult<QuickOrderResponse>> CreateQuickOrder([FromBody] QuickOrderRequest orderRequest)
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var ordersService = new OrdersService(accessToken, _linnworksSettings.DataApiUrl);
                QuickOrderResponse orderNumResponse = await ordersService.CreateQuickOrder(orderRequest);

                return Created(nameof(CreateQuickOrder), orderNumResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(CreateQuickOrder)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("getFilteredOrders")]
        public async Task<ActionResult<List<FilteredOrderDto>>> GetFilteredOrders(
            [FromQuery] string location,
            [FromQuery] string source,
            [FromQuery] string dateFrom,
            [FromQuery] string dateTo,
            [FromQuery] StatusTypes status,
            [FromQuery] FilterTypes filterBy,
            [FromQuery] bool processed,
            [FromQuery] string itemId,
            [FromQuery] string subSource,
            [FromQuery] string country,
            [FromQuery] int ordersAmount)
        {
            try
            {
                if(string.IsNullOrWhiteSpace(dateFrom) || 
                    string.IsNullOrWhiteSpace(dateTo) || 
                    string.IsNullOrWhiteSpace(location) || 
                    string.IsNullOrWhiteSpace(source))
                {
                    return BadRequest("Some fields aren't set.");
                }

                var accessToken = HttpContext.Request.Headers["Authorization"];
                var ordersService = new OrdersService(accessToken, _linnworksSettings.DataApiUrl);
                var filteredOrders = await ordersService.GetFilteredOrders(location, source, subSource, country, dateFrom, dateTo, status, filterBy, itemId, processed, ordersAmount);

                return Ok(filteredOrders);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetFilteredOrders)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("{orderId}")]
        public async Task<ActionResult<OrderAdditionalInfoDto>> GetAdditionalOrderInfo(Guid orderId)
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var ordersService = new OrdersService(accessToken, _linnworksSettings.DataApiUrl);
                var orderInfo = await ordersService.GetAdditionalOrderInfo(orderId);
                var mappedOrderInfo = _mapper.Map<OrderAdditionalInfoDto>(orderInfo);

                foreach(var item in mappedOrderInfo.Items)
                {
                    var itemImages = await ordersService.GetItemImages(item.ItemId);
                    var mainImage = itemImages.SingleOrDefault(x => x.IsMain);

                    if (mainImage != null)
                    {
                        var mainImageInBase64 = GeneralHelpers.ConvertUrlInBase64String(mainImage.Source);
                        item.MainImageInBase64 = mainImageInBase64;
                    }
                }

                return Ok(mappedOrderInfo);
            } 
            catch(Exception ex) 
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GetAdditionalOrderInfo)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("printInvoice/{orderId}")]
        public async Task<ActionResult<InvoiceResult>> PrintInvoice(Guid orderId)
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var ordersService = new OrdersService(accessToken, _linnworksSettings.DataApiUrl);
                var invoiceInBase64 = await ordersService.PrintInvoice(orderId);

                return Ok(invoiceInBase64);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(PrintInvoice)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("processOrder/{orderId}")]
        public async Task<ActionResult<ProcessOrderResult>> ProcessOrder(Guid orderId)
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var ordersService = new OrdersService(accessToken, _linnworksSettings.DataApiUrl);
                var processedResult = await ordersService.ProcessOrder(orderId);

                return Ok(processedResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(PrintInvoice)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("printShippingLabel/{orderId}")]
        public async Task<ActionResult<ShippingLabelResult>> PrintShippingLabel(Guid orderId)
        {
            try
            {
                var accessToken = HttpContext.Request.Headers["Authorization"];
                var ordersService = new OrdersService(accessToken, _linnworksSettings.DataApiUrl);
                var shippingLabelResult = await ordersService.PrintShippingLabel(orderId);

                return Ok(shippingLabelResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(PrintShippingLabel)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("createPurchaseOrder")]
        public async Task<ActionResult<CreatedPurchasesPesponse>> CreatePurchaseOrder([FromQuery] Guid orderId, [FromBody][ModelBinder(BinderType = typeof(ArrayModelBinder))] Guid[] itemsIds)
        {
            try
            {
                if(orderId.Equals(Guid.Empty) || itemsIds is null || !itemsIds.Any())
                {
                    return BadRequest("Required data is empty");
                }

                var accessToken = HttpContext.Request.Headers["Authorization"];
                var ordersService = new OrdersService(accessToken, _linnworksSettings.DataApiUrl);
                var purcahsesResponse = await ordersService.CreatePurchaseOrder(orderId, itemsIds);

                return Created(nameof(CreatePurchaseOrder), purcahsesResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(CreatePurchaseOrder)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("generateCsv")]
        public async Task<ActionResult<CsvFileResult>> GenerateOrdersCsvFile([FromBody] OrdersRequest request)
        {
            try
            {
                if (request is null)
                {
                    return BadRequest("Required data is empty");
                }

                var accessToken = HttpContext.Request.Headers["Authorization"];
                var ordersService = new OrdersService(accessToken, _linnworksSettings.DataApiUrl);
                var csvResult = await Task.Run(() => ordersService.GenerateOrdersCsvFile(request));

                return Created(nameof(GenerateOrdersCsvFile), csvResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(GenerateOrdersCsvFile)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }
    }
}

