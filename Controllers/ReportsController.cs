﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestApp.Common;
using TestApp.Dto;
using TestApp.Models;
using TestApp.Services;

namespace TestApp.Controllers
{
    [ApiController]
    [Route("api/reports")]
    public class ReportsController: ControllerBase
    {
        private readonly ILogger<ReportsController> _logger;
        private readonly LinnworksSettings _linnworksSettings;
        public ReportsController(ILogger<ReportsController> logger, LinnworksSettings linnworksSettings)
        {
            _logger = logger;
            _linnworksSettings = linnworksSettings;
        }

        [HttpGet("italyPaidOrders")]
        public async Task<ActionResult<List<ItalyPaidOrder_1_1_Model>>> ItalyPaidOrdersReport([FromQuery] string folder)
        {
            if (string.IsNullOrWhiteSpace(folder)) return BadRequest("Folder isn't wrote.");

            var authorizationToken = HttpContext.Request.Headers["Authorization"];

            try
            {
                var reportService = new ReportService(authorizationToken, _linnworksSettings.DataApiUrl);
                var response = await reportService.GetPaidItalyOrdersReport(folder);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(ItalyPaidOrdersReport)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("purchaseOrdersArrivedToday")]
        public async Task<ActionResult<List<PurchaseOrder_1_3_Model>>> PurchaseOrdersWhichWillBeArrivedTodayReport()
        {
            var authorizationToken = HttpContext.Request.Headers["Authorization"];

            try
            {
                var reportService = new ReportService(authorizationToken, _linnworksSettings.DataApiUrl);
                var response = await reportService.PurchaseOrdersWhichWillBeArrivedTodayReport();

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(PurchaseOrdersWhichWillBeArrivedTodayReport)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("startWarsItemsReport")]
        public async Task<ActionResult<List<StartWarsItem_1_6_Model>>> StartWarsItemsReport([FromBody] StarWarsItemsReportRequest request)
        {
            var authorizationToken = HttpContext.Request.Headers["Authorization"];

            try
            {
                var reportService = new ReportService(authorizationToken, _linnworksSettings.DataApiUrl);

                var response = await reportService.GetStartWarsItemsReport(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(StartWarsItemsReport)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("returnsAndRefundsReport")]
        public async Task<ActionResult<List<ReturnsRefunds_1_8_Model>>> ReturnsAndRefundsReport([FromBody] ReturnsAndRefundsReportRequest request)
        {
            var authorizationToken = HttpContext.Request.Headers["Authorization"];

            try
            {
                var reportService = new ReportService(authorizationToken, _linnworksSettings.DataApiUrl);

                var response = await reportService.GetReturnsAndRefundsReport(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(ReturnsAndRefundsReport)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("stockReport")]
        public async Task<ActionResult<List<StockReport_1_9_Model>>> StockReport([FromBody] StockReportRequest request)
        {
            var authorizationToken = HttpContext.Request.Headers["Authorization"];

            try
            {
                var reportService = new ReportService(authorizationToken, _linnworksSettings.DataApiUrl);

                var response = await reportService.GetStockReport(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(StockReport)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet("itemsByDefaultSupplier")]
        public async Task<ActionResult<List<ItemsByDefaultSuppReport_1_10_Model>>> ItemsByDefaultSupplierReport([FromQuery] string supplier = "Default")
        {
            var authorizationToken = HttpContext.Request.Headers["Authorization"];

            try
            {
                var reportService = new ReportService(authorizationToken, _linnworksSettings.DataApiUrl);

                var response = await reportService.GetItemsByDefaultSupplierReport(supplier);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(ItemsByDefaultSupplierReport)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpPost("purchaseOrdersByItem")]
        public async Task<ActionResult<List<PurchaseAsDeliveredSoonReport_1_11_Model>>> PurchaseOrdersByItemAsDeliveredSoon([FromBody] PurchaseOrdersByItemReportRequest request)
        {
            var authorizationToken = HttpContext.Request.Headers["Authorization"];

            try
            {
                var reportService = new ReportService(authorizationToken, _linnworksSettings.DataApiUrl);

                var response = await reportService.GetPurchaseOrdersByItemAsDeliveredSoon(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception has been threw. Action: {nameof(PurchaseOrdersByItemAsDeliveredSoon)} Message -> {ex.Message}");
                return StatusCode(500);
            }
        }
    }
}
    